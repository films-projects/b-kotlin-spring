plugins {
    alias(libs.plugins.springBootDependencyManagement)
    alias(libs.plugins.kotlinSpring)
    alias(libs.plugins.kotlinJvm)
    alias(libs.plugins.kotlinKapt)
    alias(libs.plugins.kotlinter)
}

dependencies {
    implementation(libs.filmsExternalInfo)

    implementation(libs.bundles.springBootCommon)
    implementation(libs.springBootCache)

    implementation(libs.bundles.caching)
    implementation(libs.mysqlConnector)

    kapt(libs.hibernateJpaModelgen)

    testImplementation(libs.bundles.testing)

//    testImplementation(libs.h2)
}

tasks.withType<Test> {
    useJUnitPlatform()
}