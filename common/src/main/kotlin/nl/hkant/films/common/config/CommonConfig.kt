package nl.hkant.films.common.config

import nl.hkant.films.common.dao.ConfigDao
import nl.hkant.films.common.model.ConfigKey
import nl.hkant.films.external.IImdbRatingService
import nl.hkant.films.external.IStreamingProviderLinkService
import nl.hkant.films.external.movieOfTheNight.MovieOfTheNightService
import nl.hkant.films.external.omdb.OmdbService
import nl.hkant.films.external.tmdb.TmdbService
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@EntityScan("nl.hkant.films.common.model")
@ComponentScan("nl.hkant.films.common")
class CommonConfig(
    private val configDao: ConfigDao,
) {

    // IExternalContentInfoFetchService
    // IStreamingProviderService
    // IEpisodeInfoService
    @Bean
    fun tmdbService(): TmdbService =
        TmdbService(
            tmdbApiKey = configDao.getRequired(ConfigKey.TMDB_API_KEY),
        )

    @Bean
    fun streamingProviderLinkService(): IStreamingProviderLinkService =
        MovieOfTheNightService(
            movieOfTheNightApiKey = configDao.getOrDefault(
                ConfigKey.MOVIE_OF_THE_NIGHT_API_KEY,
                "c46a4ffb0bmshdacf61ae3b2ee5dp194275jsnd6373e239d02",
            ),
        )

    @Bean
    fun imdbRatingService(): IImdbRatingService =
        OmdbService(omdbApiKey = configDao.getRequired(ConfigKey.OMDB_API_KEY))
}
