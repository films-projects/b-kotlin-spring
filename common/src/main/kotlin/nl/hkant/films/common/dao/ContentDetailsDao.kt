package nl.hkant.films.common.dao

import nl.hkant.films.common.model.content.ContentDetails
import nl.hkant.films.common.model.content.ContentDetails_
import nl.hkant.films.common.model.content.MovieDetails
import nl.hkant.films.common.model.content.SeriesDetails
import nl.hkant.films.external.ContentType
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
class ContentDetailsDao : AbstractDao<ContentDetails, Int>() {

    override val persistentClass = ContentDetails::class.java

    fun getRandomBackdrop(): String? {
        val hql = """
            SELECT t.backdropPath 
            FROM MovieDetails t 
            where backdropPath IS NOT NULL
            ORDER BY rand()
        """.trimIndent()
        return getSession()
            .createQuery(hql, String::class.java)
            .setMaxResults(1)
            .singleResultOrNull
    }

    fun getOneLastRefreshBefore(date: LocalDate): ContentDetails? {
        val builder = getSession().criteriaBuilder
        val query = builder.createQuery(ContentDetails::class.java)
        val root = query.from(ContentDetails::class.java)
        query.where(
            builder.or(
                builder.isNull(root.get(ContentDetails_.lastRefresh)),
                builder.lessThan(root.get(ContentDetails_.lastRefresh), date),
            ),
        )
        return getSession()
            .createQuery(query)
            .setMaxResults(1)
            .singleResultOrNull
    }

    fun getExistingContentDetails(externalContentId: Int, type: ContentType): ContentDetails? =
        when (type) {
            ContentType.FILM -> findContentDetailsByExternalContentId(externalContentId, MovieDetails::class.java)
            ContentType.SERIES -> findContentDetailsByExternalContentId(externalContentId, SeriesDetails::class.java)
        }

    private fun <T : ContentDetails> findContentDetailsByExternalContentId(externalContentId: Int, type: Class<T>): T? {
        val builder = getSession().criteriaBuilder
        val query = builder.createQuery(type)
        val root = query.from(type)
        query.where(
            builder.equal(root.get(ContentDetails_.externalContentId), externalContentId),
        )
        query.orderBy(builder.asc(root.get(ContentDetails_.id)))
        return getSession()
            .createQuery(query)
            .setMaxResults(1)
            .singleResultOrNull
    }
}
