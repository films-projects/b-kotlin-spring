package nl.hkant.films.common.services

import nl.hkant.films.common.dao.ContentDao
import nl.hkant.films.common.dao.ContentDetailsDao
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.Content
import nl.hkant.films.common.model.content.Series
import nl.hkant.films.common.model.content.WatchOption.Companion.toWatchOption
import nl.hkant.films.common.util.convertToContent
import nl.hkant.films.external.ContentType
import nl.hkant.films.external.IExternalContentInfoFetchService
import nl.hkant.films.external.IImdbRatingService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ContentService(
    private val contentDao: ContentDao,
    private val externalContentService: IExternalContentInfoFetchService,
    private val externalInfoCachingService: ExternalInfoCachingService,
    private val imdbRatingService: IImdbRatingService,
    private val contentDetailsDao: ContentDetailsDao,
) {

    @Transactional
    fun delete(user: User, id: Int) {
        val content = contentDao.findByIdAndUserIdOrException(user, id)
        content.contentDetails?.also { cd ->
            cd.content.remove(content)
            if (cd.content.isEmpty()) {
                contentDetailsDao.delete(cd)
            }
        }
        contentDao.delete(content)
    }

    @Transactional
    fun update(
        user: User,
        id: Int,
        title: String? = null,
        year: Int? = null,
        rating: Int? = null,
        watching: Boolean? = null,
        specialInterest: Boolean? = null,
    ) =
        contentDao.findByIdAndUserIdOrException(user, id)
            .apply {
                title?.also { this.title = it }
                year?.also { this.year = it }
                rating?.also {
                    this.myRating = it
                    this.seen = it != 0
                    if (it != 0) {
                        this.anticipated = false
                    }
                }
                watching?.also {
                    if (this is Series) {
                        this.watching = it
                        if (it) {
                            anticipated = false
                        }
                    }
                }
                specialInterest?.also {
                    if (!this.seen && this.myRating == 0 && !(this is Series && this.watching)) {
                        this.anticipated = it
                    }
                }
            }
            .also { contentDao.save(it) }

    @Transactional
    fun createFromExternalResource(user: User, externalContentId: Int, type: ContentType, interested: Boolean): Content? =
        externalContentService.findByExternalContentId(externalContentId, type)
            ?.convertToContent()
            ?.apply {
                contentDetails = contentDetails?.let {
                    contentDetailsDao.getExistingContentDetails(it.externalContentId, contentType)
                        ?: it.apply {
                            watchOptions = externalInfoCachingService.getStreamingProviders(externalContentId, contentType)
                                .map { sp -> sp.toWatchOption() }
                                .toMutableSet()
                            imdbId?.also { imdbId ->
                                it.imdbRating = imdbRatingService.getImdbRating(imdbId)
                            }
                        }
                }
            }
            ?.let {
                it.userId = user.id
                it.anticipated = interested
                contentDao.save(it)
            }
}
