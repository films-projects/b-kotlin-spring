package nl.hkant.films.common.util

import nl.hkant.films.common.dao.ConfigDao
import nl.hkant.films.common.model.ConfigKey
import org.springframework.stereotype.Component

const val POSTER_URL = "https://image.tmdb.org/t/p/w300/"

fun String.toPosterImageUrl() = "$POSTER_URL${this.removePrefix("/")}"

@Component
class ImageUtil(
    private val configDao: ConfigDao,
) {
    fun toPosterImageUrl(path: String) =
        "${configDao.getRequired(ConfigKey.TMDB_IMAGE_BASEURL_POSTER)}${path.removePrefix("/")}"

    fun toBackgroundImageUrl(path: String) =
        "${configDao.getRequired(ConfigKey.TMDB_IMAGE_BASEURL_BACKDROP)}${path.removePrefix("/")}"
}
