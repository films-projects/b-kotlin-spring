package nl.hkant.films.common.services

import nl.hkant.films.common.config.CacheConfig
import nl.hkant.films.external.ContentType
import nl.hkant.films.external.IEpisodeInfoService
import nl.hkant.films.external.IStreamingProviderService
import nl.hkant.films.external.LastAndNextEpisode
import nl.hkant.films.external.StreamingProvider
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service

@Service
class ExternalInfoCachingService(
    private val streamingProviderService: IStreamingProviderService,
    private val episodeInfoService: IEpisodeInfoService,
) {

    @Cacheable(cacheNames = [CacheConfig.CacheNames.WATCH_OPTIONS], key = "#externalContentId")
    fun getStreamingProviders(externalContentId: Int, contentType: ContentType): MutableSet<StreamingProvider> =
        streamingProviderService.getStreamingProviders(externalContentId, contentType)

    @Cacheable(cacheNames = [CacheConfig.CacheNames.EPISODE_INFO], key = "#externalContentId")
    fun getLastAndUpcomingEpisodes(externalContentId: Int): LastAndNextEpisode =
        episodeInfoService.getLastAndUpcomingEpisodes(externalContentId)
}
