package nl.hkant.films.common.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "Configuration")
open class Config(
    @Id
    @Column(name = "configKey")
    open val key: String,
    @Column(name = "configValue")
    open val value: String?,
) {
    constructor() : this("", null)
}
