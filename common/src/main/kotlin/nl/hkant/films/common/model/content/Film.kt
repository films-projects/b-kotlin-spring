package nl.hkant.films.common.model.content

import jakarta.persistence.CascadeType
import jakarta.persistence.DiscriminatorValue
import jakarta.persistence.Entity
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table
import jakarta.persistence.Transient
import nl.hkant.films.external.ContentType

@Entity
@DiscriminatorValue("FILM")
@Table(name = "Content")
class Film(
    contentDetails: MovieDetails? = null,
    id: Int = 0,
    title: String = "",
    year: Int = 0,
    seen: Boolean = false,
    anticipated: Boolean = false,
    rating: Int = 0,
    userId: Int = 0,
) : Content(
    id = id,
    title = title,
    year = year,
    seen = seen,
    anticipated = anticipated,
    myRating = rating,
    contentDetails = contentDetails,
    userId = userId,
) {
    @ManyToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "contentDetailsId", updatable = false, insertable = false)
    var movieDetails: MovieDetails? = null

    @Transient
    override fun getContentInfo() = contentDetails as? MovieDetails

    @Transient
    override val contentType = ContentType.FILM
}
