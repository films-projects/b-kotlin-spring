package nl.hkant.films.common.util

import nl.hkant.films.common.model.content.Film
import nl.hkant.films.common.model.content.MovieDetails
import nl.hkant.films.common.model.content.Series
import nl.hkant.films.common.model.content.SeriesDetails
import nl.hkant.films.common.model.content.SeriesStatus.Companion.toSeriesStatus
import nl.hkant.films.external.results.FilmResult
import nl.hkant.films.external.results.IContentResult
import nl.hkant.films.external.results.SeriesResult

fun IContentResult.convertToContent() =
    when (this) {
        is FilmResult ->
            Film(
                title = this.title,
                year = this.year,
                contentDetails = this.toMovieDetails(),
            )
        is SeriesResult ->
            Series(
                title = this.title,
                year = this.year,
                contentDetails = this.toSeriesDetails(),
            )
        else -> throw IllegalArgumentException("Unknown type encountered")
    }

fun IContentResult.convertToContentDetails() =
    when (this) {
        is FilmResult ->
            toMovieDetails()
        is SeriesResult ->
            toSeriesDetails()
        else -> throw IllegalArgumentException("Unknown type encountered")
    }

fun FilmResult.toMovieDetails() =
    MovieDetails(
        airDate = this.airDate,
        runtime = this.runtime,
        externalContentId = this.externalContentId,
        posterPath = this.posterPath,
        backdropPath = this.backdropPath,
        overview = this.overview,
        imdbId = this.imdbId,
    )

fun SeriesResult.toSeriesDetails() =
    SeriesDetails(
        airDate = this.firstAirDate,
        lastAirDate = this.lastAirDate,
        episodeRuntime = this.episodeRuntime,
        numSeasons = this.numSeasons,
        numEpisodes = this.numEpisodes,
        status = this.status.toSeriesStatus(),
        externalContentId = this.externalContentId,
        posterPath = this.posterPath,
        backdropPath = this.backdropPath,
        overview = this.overview,
        imdbId = this.imdbId,
    )
