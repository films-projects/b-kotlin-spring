package nl.hkant.films.common.model.content

import jakarta.persistence.CollectionTable
import jakarta.persistence.Column
import jakarta.persistence.DiscriminatorColumn
import jakarta.persistence.DiscriminatorType
import jakarta.persistence.ElementCollection
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Inheritance
import jakarta.persistence.InheritanceType
import jakarta.persistence.JoinColumn
import jakarta.persistence.OneToMany
import jakarta.persistence.Table
import java.time.LocalDate

@Entity
@Table(name = "ContentDetails")
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Inheritance(strategy = InheritanceType.JOINED)
abstract class ContentDetails(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Int = 0,
    open var externalContentId: Int = 0,
    open var posterPath: String? = null,
    open var backdropPath: String? = null,

    @Column(length = 2000)
    open var overview: String? = null,

    @Column(length = 15)
    open var imdbId: String? = null,
    open var imdbRating: Double? = null,
    open var lastRefresh: LocalDate? = null,

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "content_watch_options", joinColumns = [JoinColumn(name = "content_id")])
    @Column(name = "watch_option")
    @Enumerated(EnumType.STRING)
    open var watchOptions: MutableSet<WatchOption> = mutableSetOf(),

    // eager fetch did not work with List for some reason
    @OneToMany(targetEntity = Content::class, mappedBy = "contentDetails", fetch = FetchType.EAGER)
    open var content: MutableSet<Content> = mutableSetOf(),
) {

    @Transient
    open var contentTitle: String? = null

    open fun getReleaseDate(): LocalDate? = null
}
