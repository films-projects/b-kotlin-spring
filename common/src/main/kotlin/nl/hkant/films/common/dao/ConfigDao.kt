package nl.hkant.films.common.dao

import nl.hkant.films.common.model.Config
import nl.hkant.films.common.model.ConfigKey
import nl.hkant.films.common.model.NotFoundException
import org.springframework.stereotype.Repository

@Repository
class ConfigDao : AbstractDao<Config, String>() {

    override val persistentClass = Config::class.java

    fun getRequired(configKey: ConfigKey) = findById(configKey.key)?.value ?: throw NotFoundException()

    fun getOrDefault(configKey: ConfigKey, default: String) = findById(configKey.key)?.value ?: default
}
