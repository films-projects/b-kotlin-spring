package nl.hkant.films.common.model.content

import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.DiscriminatorColumn
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Inheritance
import jakarta.persistence.InheritanceType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table
import jakarta.persistence.Transient
import nl.hkant.films.external.ContentType

@Entity
@Table(name = "Content")
@DiscriminatorColumn(name = "type")
@Inheritance(strategy = InheritanceType.JOINED)
abstract class Content(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Int = 0,
    open var title: String = "",
    open var year: Int = 0,
    open var userId: Int = 0,
    open var seen: Boolean = false,
    open var anticipated: Boolean = false,

    @Column(nullable = false, name = "rating")
    open var myRating: Int = 0,

    @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH], fetch = FetchType.EAGER)
    @JoinColumn(name = "contentDetailsId")
    open var contentDetails: ContentDetails? = null,
) {

    open fun copyFrom(other: Content) {
        title = other.title
        year = other.year
        seen = other.seen
        anticipated = other.anticipated
        myRating = other.myRating
    }

    override fun toString(): String {
        return "Film [id=$id, title=$title, year=$year, seen=$seen, myRating=$myRating, details=${contentDetails?.id}]"
    }

    @Transient
    open fun getContentInfo() = contentDetails

    abstract val contentType: ContentType
}
