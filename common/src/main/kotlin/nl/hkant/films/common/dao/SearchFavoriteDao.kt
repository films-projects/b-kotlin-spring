package nl.hkant.films.common.dao

import nl.hkant.films.common.model.search.SearchFavorite
import nl.hkant.films.common.model.search.SearchFavorite_
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
class SearchFavoriteDao : AbstractDao<SearchFavorite, Int>() {

    override val persistentClass = SearchFavorite::class.java

    @Transactional(readOnly = true)
    fun findByUserIdReadOnly(userId: Int): List<SearchFavorite> {
        val builder = getSession().criteriaBuilder
        val query = builder.createQuery(SearchFavorite::class.java)
        val root = query.from(SearchFavorite::class.java)
        query.where(builder.equal(root.get(SearchFavorite_.userId), userId))
        return getSession().createQuery(query).list()
    }

    fun removeByQuery(userId: Int, queryString: String) {
        val builder = getSession().criteriaBuilder
        val deleteQuery = builder.createCriteriaDelete(SearchFavorite::class.java)
        val root = deleteQuery.from(SearchFavorite::class.java)
        deleteQuery.where(
            builder.equal(root.get(SearchFavorite_.userId), userId),
            builder.equal(root.get(SearchFavorite_.query), queryString),
        )
        getSession().createMutationQuery(deleteQuery).executeUpdate()
    }
}
