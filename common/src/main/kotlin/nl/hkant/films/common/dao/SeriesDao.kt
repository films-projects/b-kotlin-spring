package nl.hkant.films.common.dao

import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.Series
import nl.hkant.films.common.model.content.Series_
import org.springframework.stereotype.Repository

@Repository
class SeriesDao : AbstractDao<Series, Int>() {

    override val persistentClass = Series::class.java

    fun findByWatchingOrAnticipatedIsTrue(user: User): List<Series> {
        val builder = entityManager.criteriaBuilder
        val query = builder.createQuery(Series::class.java)
        val root = query.from(Series::class.java)
        query.where(
            builder.equal(root.get(Series_.userId), user.id),
            builder.or(
                builder.isTrue(root.get(Series_.watching)),
                builder.isTrue(root.get(Series_.anticipated)),
            ),
        )
        return entityManager.createQuery(query).resultList
    }
}
