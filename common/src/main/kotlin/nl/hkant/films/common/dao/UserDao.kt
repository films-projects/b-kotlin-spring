package nl.hkant.films.common.dao

import nl.hkant.films.common.model.User
import org.springframework.stereotype.Repository

@Repository
class UserDao : AbstractDao<User, Int>() {

    override val persistentClass = User::class.java

    fun findByUsername(username: String): User? =
        getSession()
            .createQuery("SELECT u FROM User u where username = :username", User::class.java)
            .setParameter("username", username)
            .uniqueResult()

    fun findByPrivateKey(privateKey: String): User? =
        getSession()
            .createQuery("SELECT u FROM User u where privateKey = :privateKey", User::class.java)
            .setParameter("privateKey", privateKey)
            .uniqueResult()
}
