package nl.hkant.films.common.model.content

import nl.hkant.films.external.StreamingProvider

enum class WatchOption(val friendlyName: String) {
    NETFLIX("Netflix"),
    AMAZON_PRIME("Amazon Prime Video"),
    APPLE_TV("Apple TV+"),
    HBO_MAX("HBO Max"),
    SKY_SHOWTIME("Sky Showtime"),
    ;

    fun toStreamingProvider() =
        when (this) {
            AMAZON_PRIME -> StreamingProvider.AMAZON_PRIME
            APPLE_TV -> StreamingProvider.APPLE_TV
            HBO_MAX -> StreamingProvider.HBO_MAX
            NETFLIX -> StreamingProvider.NETFLIX
            SKY_SHOWTIME -> StreamingProvider.SKY_SHOWTIME
        }

    companion object {
        fun StreamingProvider.toWatchOption() =
            when (this) {
                StreamingProvider.AMAZON_PRIME -> AMAZON_PRIME
                StreamingProvider.APPLE_TV -> APPLE_TV
                StreamingProvider.HBO_MAX -> HBO_MAX
                StreamingProvider.NETFLIX -> NETFLIX
                StreamingProvider.SKY_SHOWTIME -> SKY_SHOWTIME
            }
    }
}
