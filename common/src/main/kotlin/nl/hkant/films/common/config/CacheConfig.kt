package nl.hkant.films.common.config

import org.springframework.cache.annotation.EnableCaching
import org.springframework.cache.jcache.JCacheCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.TimeUnit
import javax.cache.CacheManager
import javax.cache.Caching
import javax.cache.configuration.MutableConfiguration
import javax.cache.expiry.CreatedExpiryPolicy
import javax.cache.expiry.Duration

@Configuration
@EnableCaching
class CacheConfig {

    @Bean
    fun jCacheCacheManager(): JCacheCacheManager =
        JCacheCacheManager(cacheManager())

    @Bean
    fun cacheManager(): CacheManager {
        val cacheManager = Caching.getCachingProvider().cacheManager
        fun configForExpiryInDays(days: Long) = MutableConfiguration<String, Any>()
            .setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(Duration(TimeUnit.DAYS, days)))
        val defaultConfig = configForExpiryInDays(7)
        cacheManager.createCache(CacheNames.WATCH_OPTIONS, defaultConfig)
        cacheManager.createCache(CacheNames.EPISODE_INFO, configForExpiryInDays(1))
        cacheManager.createCache(CacheNames.WATCHING_EPISODES, configForExpiryInDays(1))
        return cacheManager
    }

    class CacheNames {
        companion object {
            const val WATCH_OPTIONS = "watchOptions"
            const val EPISODE_INFO = "episodeInfo"
            const val WATCHING_EPISODES = "watchingEpisodes"
        }
    }
}
