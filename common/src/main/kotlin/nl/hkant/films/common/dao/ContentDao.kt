package nl.hkant.films.common.dao

import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root
import nl.hkant.films.common.model.NotFoundException
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.Content
import nl.hkant.films.common.model.content.ContentDetails
import nl.hkant.films.common.model.content.ContentDetails_
import nl.hkant.films.common.model.content.Content_
import nl.hkant.films.common.model.content.Film
import nl.hkant.films.common.model.content.Film_
import nl.hkant.films.common.model.content.MovieDetails
import nl.hkant.films.common.model.content.MovieDetails_
import nl.hkant.films.common.model.content.Series
import nl.hkant.films.common.model.content.SeriesDetails
import nl.hkant.films.common.model.content.SeriesDetails_
import nl.hkant.films.common.model.content.SeriesStatus
import nl.hkant.films.common.model.content.Series_
import nl.hkant.films.common.model.content.WatchOption
import nl.hkant.films.external.ContentType
import org.hibernate.query.SortDirection
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
class ContentDao : AbstractDao<Content, Int>() {

    override val persistentClass = Content::class.java

    fun findByIdAndUserIdOrException(user: User, id: Int): Content {
        val builder = getSession().criteriaBuilder
        val query = builder.createQuery(Content::class.java)
        val root = query.from(Content::class.java)
        query.where(
            builder.equal(root.get(Content_.userId), user.id),
            builder.equal(root.get(Content_.id), id),
        )
        return getSession().createQuery(query).uniqueResult()
            ?: throw NotFoundException("Content $id does not exist for user ${user.id}")
    }

    fun findByIdAndUserId(user: User, id: Int): Content? {
        val builder = getSession().criteriaBuilder
        val query = builder.createQuery(Content::class.java)
        val root = query.from(Content::class.java)
        query.where(
            builder.equal(root.get(Content_.userId), user.id),
            builder.equal(root.get(Content_.id), id),
        )
        return getSession().createQuery(query).uniqueResult()
    }

    fun findByParams(
        user: User,
        pageable: Pageable,
        title: String? = null,
        yearFrom: Int? = null,
        yearTo: Int? = null,
        anticipated: Boolean? = null,
        myRating: List<Int>? = null,
        type: String? = null,
        minRating: Double? = null,
        released: Boolean? = null,
        watching: Boolean? = null,
        seriesStatus: SeriesStatus? = null,
        watchOptions: List<WatchOption>? = null,
    ): Page<Content> {
        val builder = getSession().criteriaBuilder
        val qry = builder.createQuery(Content::class.java)
        val root = if (type?.lowercase() == "film") {
            qry.from(Film::class.java)
        } else if (type?.lowercase() == "series") {
            qry.from(Series::class.java)
        } else {
            qry.from(Content::class.java)
        }

        val predicates = createPredicates(
            builder = builder,
            root = root,
            user = user,
            title = title,
            yearFrom = yearFrom,
            yearTo = yearTo,
            anticipated = anticipated,
            myRating = myRating,
            watching = watching,
            minRating = minRating,
            seriesStatus = seriesStatus,
            released = released,
            watchOptions = watchOptions,
        )
        qry.where(*predicates.toTypedArray())

        val (orderAttribute, orderDirection) = if (pageable.sort.isUnsorted) {
            root.get<String>(Content_.TITLE) to Sort.Direction.ASC.toString()
        } else if (pageable.sort.first().property == "rating") {
            root
                .get<ContentDetails>(Content_.CONTENT_DETAILS)
                .get(ContentDetails_.imdbRating) to pageable.sort.first().direction.toString()
        } else {
            root.get<Any>(pageable.sort.first().property) to pageable.sort.first().direction.toString()
        }
        qry.orderBy(
            builder.sort(
                orderAttribute,
                SortDirection.interpret(orderDirection),
            ),
        )
        val query = getSession().createQuery(qry)

        val total = countWithPredicates(
            user = user,
            title = title,
            yearFrom = yearFrom,
            yearTo = yearTo,
            anticipated = anticipated,
            myRating = myRating,
            type = type,
            released = released,
            watching = watching,
            minRating = minRating,
            seriesStatus = seriesStatus,
            watchOptions = watchOptions,
        )
        val list = query
            .setFirstResult(pageable.pageNumber * pageable.pageSize)
            .setMaxResults(pageable.pageSize)
            .setReadOnly(true)
            .resultList

        return PageImpl(list, pageable, total.toLong())
    }

    private fun countWithPredicates(
        user: User,
        title: String? = null,
        yearFrom: Int? = null,
        yearTo: Int? = null,
        anticipated: Boolean? = null,
        myRating: List<Int>? = null,
        type: String? = null,
        released: Boolean? = null,
        watching: Boolean? = null,
        minRating: Double? = null,
        seriesStatus: SeriesStatus? = null,
        watchOptions: List<WatchOption>? = null,
    ): Int {
        val builder = getSession().criteriaBuilder
        val qry = builder.createQuery(Long::class.java)
        val root = if (type?.lowercase() == "film") {
            qry.from(Film::class.java)
        } else if (type?.lowercase() == "series") {
            qry.from(Series::class.java)
        } else {
            qry.from(Content::class.java)
        }

        val predicates = createPredicates(
            builder = builder,
            root = root,
            user = user,
            title = title,
            yearFrom = yearFrom,
            yearTo = yearTo,
            anticipated = anticipated,
            myRating = myRating,
            watching = watching,
            minRating = minRating,
            seriesStatus = seriesStatus,
            released = released,
            watchOptions = watchOptions,
        )
        qry.select(builder.count(root))
        if (predicates.isNotEmpty()) {
            qry.where(*predicates.toTypedArray())
        }
        return getSession().createQuery(qry).singleResult.toInt()
    }

    private fun createPredicates(
        builder: CriteriaBuilder,
        root: Root<out Content>,
        user: User,
        title: String? = null,
        yearFrom: Int? = null,
        yearTo: Int? = null,
        anticipated: Boolean? = null,
        myRating: List<Int>? = null,
        watching: Boolean? = null,
        released: Boolean? = null,
        minRating: Double? = null,
        seriesStatus: SeriesStatus? = null,
        watchOptions: List<WatchOption>? = null,
    ): List<Predicate> {
        return listOfNotNull(
            builder.equal(root.get<Int>(Content_.USER_ID), user.id),
            title?.let { builder.like(root.get(Content_.TITLE), "%$it%") },
            anticipated?.let { builder.equal(root.get<Boolean>(Content_.ANTICIPATED), it) },
            watching?.let { builder.equal(root.get<Boolean>(Series_.WATCHING), it) },
            yearFrom?.let { builder.greaterThanOrEqualTo(root.get(Content_.YEAR), it) },
            yearTo?.let { builder.lessThanOrEqualTo(root.get(Content_.YEAR), it) },
            minRating?.let {
                builder.greaterThanOrEqualTo(
                    root.get<Any>(Content_.CONTENT_DETAILS).get(
                        ContentDetails_.IMDB_RATING,
                    ),
                    it,
                )
            },
            myRating?.takeUnless { it.isEmpty() }?.let { root.get<Int>(Content_.MY_RATING).`in`(it) },
            seriesStatus?.let {
                builder.equal(
                    root.get<SeriesDetails>(Content_.CONTENT_DETAILS).get<SeriesStatus>(
                        SeriesDetails_.STATUS,
                    ),
                    seriesStatus,
                )
            },
            released?.let {
                val now = LocalDate.now()
                if (it) {
                    builder.lessThanOrEqualTo(
                        root.get<MovieDetails>(Film_.MOVIE_DETAILS).get(
                            MovieDetails_.AIR_DATE,
                        ),
                        now,
                    )
                } else {
                    builder.greaterThan(
                        root.get<MovieDetails>(Film_.MOVIE_DETAILS).get(
                            MovieDetails_.AIR_DATE,
                        ),
                        now,
                    )
                }
            },
            watchOptions?.takeUnless { it.isEmpty() }?.let {
                val contentDetailsJoin = root.join<Content, ContentDetails>(Content_.CONTENT_DETAILS)
                val optionsJoin = contentDetailsJoin.join<ContentDetails, WatchOption>(ContentDetails_.WATCH_OPTIONS)
                optionsJoin.`in`(it)
            },
        )
    }

    fun findByExternalContentIdForUser(user: User, externalContentId: Int, contentType: ContentType): Content? {
        val clazz = when (contentType) {
            ContentType.FILM -> Film::class.java
            ContentType.SERIES -> Series::class.java
        }
        val builder = getSession().criteriaBuilder
        val query = builder.createQuery(clazz)
        val root = query.from(clazz)
        query.where(
            builder.equal(root.get(Content_.userId), user.id),
            builder.equal(
                root.get(Content_.contentDetails).get(
                    ContentDetails_.externalContentId,
                ),
                externalContentId,
            ),
        )
        return getSession()
            .createQuery(query)
            .setMaxResults(1)
            .singleResultOrNull
    }

    fun findWatchingOngoingAndUpcomingAnticipatedSeries(user: User): List<Series> {
        val builder = entityManager.criteriaBuilder
        val query = builder.createQuery(Series::class.java)
        val root = query.from(Series::class.java)
        query.where(
            builder.equal(root.get(Content_.userId), user.id),
            builder.or(
                builder.and(
                    builder.isTrue(root.get(Series_.watching)),
                    builder.or(
                        builder.equal(root.get(Series_.seriesDetails).get(SeriesDetails_.status), SeriesStatus.ONGOING),
                    ),
                ),
                builder.and(
                    builder.isTrue(root.get(Series_.anticipated)),
                    builder.equal(root.get(Series_.seriesDetails).get(SeriesDetails_.status), SeriesStatus.UPCOMING),
                ),
            ),
        )
        return entityManager
            .createQuery(query)
            .resultList
    }

    fun getUpcomingFilmsInUpcomingMonth(
        user: User,
        startDateExcl: LocalDate,
    ): List<Film> {
        val builder = entityManager.criteriaBuilder
        val query = builder.createQuery(Film::class.java)
        val root = query.from(Film::class.java)
        query.where(
            builder.equal(
                root.get(Film_.userId),
                user.id,
            ),
            builder.isNotNull(
                root.get(Film_.movieDetails).get(MovieDetails_.airDate),
            ),
            builder.greaterThan(
                root.get(Film_.movieDetails).get(MovieDetails_.airDate),
                startDateExcl,
            ),
            builder.lessThan(
                root.get(Film_.movieDetails).get(MovieDetails_.airDate),
                startDateExcl.plusMonths(1),
            ),
        )
        return entityManager.createQuery(query).resultList
    }

    fun getWatchingOrUpcomingSeries(user: User): List<Series> {
        val builder = entityManager.criteriaBuilder
        val query = builder.createQuery(Series::class.java)
        val root = query.from(Series::class.java)
        query.where(
            builder.equal(root.get(Series_.userId), user.id),
            builder.or(
                builder.equal(
                    root.get(Series_.seriesDetails).get(SeriesDetails_.status),
                    SeriesStatus.UPCOMING,
                ),
                builder.and(
                    builder.equal(
                        root.get(Series_.seriesDetails).get(SeriesDetails_.status),
                        SeriesStatus.ONGOING,
                    ),
                    builder.isTrue(
                        root.get(Series_.watching),
                    ),
                ),
            ),
        )
        return entityManager.createQuery(query).resultList
    }
}
