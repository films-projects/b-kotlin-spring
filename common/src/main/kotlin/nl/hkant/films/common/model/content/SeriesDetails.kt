package nl.hkant.films.common.model.content

import jakarta.persistence.Column
import jakarta.persistence.DiscriminatorValue
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.PrimaryKeyJoinColumn
import java.time.LocalDate

@Entity
@DiscriminatorValue("SERIES")
@PrimaryKeyJoinColumn(name = "id")
class SeriesDetails(
    @Column(name = "firstAirDate")
    var airDate: LocalDate? = null,
    var lastAirDate: LocalDate? = null,
    var episodeRuntime: Int = 0,
    var numSeasons: Int = 0,
    var numEpisodes: Int = 0,
    @Enumerated(EnumType.STRING)
    var status: SeriesStatus = SeriesStatus.UPCOMING,

    externalContentId: Int = 0,
    posterPath: String? = null,
    backdropPath: String? = null,
    overview: String? = null,
    imdbId: String? = null,
    imdbRating: Double? = null,
) : ContentDetails(
    externalContentId = externalContentId,
    posterPath = posterPath,
    backdropPath = backdropPath,
    overview = overview,
    imdbId = imdbId,
    imdbRating = imdbRating,
) {

    override fun getReleaseDate() = airDate

    fun update(other: SeriesDetails) {
        posterPath = other.posterPath
        backdropPath = other.backdropPath
        overview = other.overview
        imdbId = other.imdbId
        airDate = other.airDate
        lastAirDate = other.lastAirDate
        numEpisodes = other.numEpisodes
        numSeasons = other.numSeasons
        episodeRuntime = other.episodeRuntime
        status = other.status
        imdbRating = other.imdbRating
        watchOptions.clear()
        watchOptions.addAll(other.watchOptions)
    }
}
