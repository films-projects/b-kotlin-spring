package nl.hkant.films.common.model.content

import jakarta.persistence.Column
import jakarta.persistence.DiscriminatorValue
import jakarta.persistence.Entity
import jakarta.persistence.PrimaryKeyJoinColumn
import java.time.LocalDate

@Entity
@DiscriminatorValue("FILM")
@PrimaryKeyJoinColumn(name = "id")
class MovieDetails(
    @Column(name = "releaseDate")
    var airDate: LocalDate? = null,
    var runtime: Int = 0,
    externalContentId: Int = 0,
    posterPath: String? = null,
    backdropPath: String? = null,
    overview: String? = null,
    imdbId: String? = null,
    imdbRating: Double? = null,
) : ContentDetails(
    externalContentId = externalContentId,
    posterPath = posterPath,
    backdropPath = backdropPath,
    overview = overview,
    imdbId = imdbId,
    imdbRating = imdbRating,
) {

    fun update(other: MovieDetails) {
        posterPath = other.posterPath
        backdropPath = other.backdropPath
        overview = other.overview
        airDate = other.airDate
        runtime = other.runtime
        imdbId = other.imdbId
        imdbRating = other.imdbRating
        watchOptions.clear()
        watchOptions.addAll(other.watchOptions)
    }

    override fun getReleaseDate() = airDate

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }
        val movieDetails = other as MovieDetails
        return externalContentId == movieDetails.externalContentId
    }

    override fun hashCode(): Int {
        var result = externalContentId
        result = 31 * result + if (posterPath != null) posterPath.hashCode() else 0
        result = 31 * result + if (overview != null) overview.hashCode() else 0
        result = 31 * result + if (imdbId != null) imdbId.hashCode() else 0
        result = 31 * result + if (airDate != null) airDate.hashCode() else 0
        result = 31 * result + runtime
        return result
    }
}
