package nl.hkant.films.common.model

enum class ConfigKey(val key: String) {
    MOVIE_OF_THE_NIGHT_API_KEY("movieOfTheNight.api.key"),
    TMDB_API_KEY("tmdb.api.key"),
    OMDB_API_KEY("omdb.token"),
    SCHEDULED_REFRESH_DAY_LIMIT("scheduled.refresh.day.expiration"),
    USABLE_WATCH_OPTIONS("watch.options.usable"),
    TMDB_IMAGE_BASEURL_BACKDROP("tmdb.image.baseurl.backdrop"),
    TMDB_IMAGE_BASEURL_POSTER("tmdb.image.baseurl.poster"),
}
