package nl.hkant.films.common.model

import jakarta.persistence.CollectionTable
import jakarta.persistence.Column
import jakarta.persistence.ElementCollection
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

@Entity
class User(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0,
    @CollectionTable(name = "UserRole", joinColumns = [JoinColumn(name = "userId")])
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    val userRoles: Set<UserRole> = emptySet(),
    private val username: String = "",
    private val password: String = "",
    val privateKey: String = "",
) : UserDetails {
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> =
        userRoles
            .map { GrantedAuthority { it.name } }
            .toMutableSet()

    override fun getPassword(): String = password

    override fun getUsername(): String = username

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true

    override fun isCredentialsNonExpired(): Boolean = true

    override fun isEnabled(): Boolean = true

    fun isAdmin() = userRoles.contains(UserRole.ROLE_ADMIN)
}
