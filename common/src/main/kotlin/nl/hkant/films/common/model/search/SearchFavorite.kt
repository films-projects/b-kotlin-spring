package nl.hkant.films.common.model.search

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "SearchFavorite")
class SearchFavorite(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0,
    val userId: Int = 0,
    val query: String = "",
) {
    fun params() =
        query.split("&")
            .map {
                val parts = it.split("=")
                Pair(parts.first(), parts.last())
            }
            .groupBy { it.first }
            .mapValues { it.value.map { value -> value.second } }
}
