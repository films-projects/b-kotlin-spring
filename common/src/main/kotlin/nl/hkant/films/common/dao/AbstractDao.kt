package nl.hkant.films.common.dao

import jakarta.persistence.EntityManager
import org.hibernate.Session
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional

@Transactional
abstract class AbstractDao<T, ID> {

    @Autowired
    lateinit var entityManager: EntityManager

    abstract val persistentClass: Class<T>

    fun findById(id: ID): T? {
        return entityManager.find(persistentClass, id)
    }

    fun save(obj: T): T {
        entityManager.persist(obj)
        return obj
    }

    fun delete(obj: T) {
        if (entityManager.contains(obj)) {
            entityManager.remove(obj)
        } else {
            entityManager.remove(entityManager.merge(obj))
        }
    }

    fun getSession(): Session {
        return entityManager.unwrap(Session::class.java)
    }
}
