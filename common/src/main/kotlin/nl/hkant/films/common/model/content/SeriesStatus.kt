package nl.hkant.films.common.model.content

enum class SeriesStatus(val friendlyName: String) {
    ONGOING("Ongoing"),
    UPCOMING("Upcoming"),
    CANCELLED("Cancelled"),
    ENDED("Ended"),
    UNKNOWN("Unknown"),
    ;

    companion object {
        fun String.toSeriesStatus(): SeriesStatus {
            return when (this) {
                "Canceled" -> CANCELLED
                "Ended" -> ENDED
                "Returning Series" -> ONGOING
                "Planned",
                "In Production",
                -> UPCOMING

                else -> UNKNOWN
            }
        }
        fun nl.hkant.films.external.SeriesStatus.toSeriesStatus(): SeriesStatus {
            return when (this) {
                nl.hkant.films.external.SeriesStatus.CANCELLED -> CANCELLED
                nl.hkant.films.external.SeriesStatus.ENDED -> ENDED
                nl.hkant.films.external.SeriesStatus.ONGOING -> ONGOING
                nl.hkant.films.external.SeriesStatus.UPCOMING -> UPCOMING
                nl.hkant.films.external.SeriesStatus.UNKNOWN -> UNKNOWN
            }
        }
    }
}
