package nl.hkant.films.common.dao

import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.JoinType
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.Content
import nl.hkant.films.common.model.content.ContentDetails
import nl.hkant.films.common.model.content.ContentDetails_
import nl.hkant.films.common.model.content.Film
import nl.hkant.films.common.model.content.Film_
import nl.hkant.films.common.model.content.MovieDetails_
import nl.hkant.films.common.model.content.WatchOption
import org.springframework.stereotype.Repository
import java.time.LocalDate
import kotlin.random.Random

@Repository
class FilmDao : AbstractDao<Film, Int>() {

    override val persistentClass = Film::class.java

    fun findSuggestion(user: User, seen: Boolean, anticipated: Boolean?, minRating: Double?, watchOptions: Set<WatchOption>?): Film? {
        val builder = getSession().criteriaBuilder

        val total = getTotalForSuggestion(
            user = user,
            seen = seen,
            anticipated = anticipated,
            minRating = minRating,
            watchOptions = watchOptions,
        )
        if (total == 0) {
            return null
        }

        val number = Random.nextInt(total)
        val query = builder.createQuery(Film::class.java)
        val film = query.from(Film::class.java)

        val predicates = getSuggestionPredicates(
            builder = builder,
            root = film,
            user = user,
            seen = seen,
            anticipated = anticipated,
            minRating = minRating,
            watchOptions = watchOptions,
        )
        query.where(*predicates.toTypedArray())
        return getSession()
            .createQuery(query)
            .setFirstResult(number)
            .setMaxResults(1)
            .uniqueResult()
    }

    private fun getTotalForSuggestion(
        user: User,
        seen: Boolean,
        anticipated: Boolean?,
        minRating: Double?,
        watchOptions: Set<WatchOption>?,
    ): Int {
        val builder = getSession().criteriaBuilder
        val query2 = builder.createQuery(Long::class.java)
        val film = query2.from(Film::class.java)
        query2.select(builder.count(film.get(Film_.id)))

        val predicates = getSuggestionPredicates(
            builder = builder,
            root = film,
            user = user,
            seen = seen,
            anticipated = anticipated,
            minRating = minRating,
            watchOptions = watchOptions,
        )
        query2.where(*predicates.toTypedArray())
        return getSession()
            .createQuery(query2)
            .setMaxResults(1)
            .uniqueResult()
            .toInt()
    }

    private fun getSuggestionPredicates(
        builder: CriteriaBuilder,
        root: Root<Film>,
        user: User,
        seen: Boolean,
        anticipated: Boolean?,
        minRating: Double?,
        watchOptions: Set<WatchOption>?,
    ): List<Predicate> {
        return listOfNotNull(
            builder.equal(root.get(Film_.userId), user.id),
            builder.equal(root.get(Film_.seen), seen),
            anticipated?.let { builder.equal(root.get(Film_.anticipated), it) },
            minRating?.let {
                builder.greaterThanOrEqualTo(
                    root.get(Film_.contentDetails).get(ContentDetails_.imdbRating),
                    minRating,
                )
            },
            watchOptions?.let {
                root.join<Film, ContentDetails>(Film_.CONTENT_DETAILS, JoinType.LEFT)
                    .join<ContentDetails, Set<WatchOption>>(ContentDetails_.WATCH_OPTIONS, JoinType.LEFT)
                    .`in`(watchOptions)
            },
            builder.lessThan(
                root.get(Film_.movieDetails).get(MovieDetails_.airDate),
                LocalDate.now(),
            ),
        )
    }

    fun getUnseenByAirDateBetween(
        user: User,
        startDateExcl: LocalDate,
        endDateIncl: LocalDate,
        filterSeen: Boolean = false,
    ): List<Content> {
        val builder = entityManager.criteriaBuilder
        val query = builder.createQuery(Film::class.java)
        val root = query.from(Film::class.java)
        val predicates = listOfNotNull(
            builder.equal(root.get(Film_.userId), user.id),
            builder.greaterThan(
                root.get(Film_.movieDetails).get(MovieDetails_.airDate),
                startDateExcl,
            ),
            builder.lessThanOrEqualTo(
                root.get(Film_.movieDetails).get(MovieDetails_.airDate),
                endDateIncl,
            ),
            filterSeen.takeIf { it }?.let { builder.equal(root.get(Film_.seen), false) },
        )
        query.where(*predicates.toTypedArray())
        return entityManager.createQuery(query).resultList
    }
}
