package nl.hkant.films.common.model

enum class UserRole {
    ROLE_ADMIN,
    ROLE_USER,
}
