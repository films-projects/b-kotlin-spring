package nl.hkant.films.common.model.content

import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.DiscriminatorValue
import jakarta.persistence.Entity
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.SecondaryTable
import jakarta.persistence.Table
import jakarta.persistence.Transient
import nl.hkant.films.external.ContentType

@Entity
@DiscriminatorValue("SERIES")
@Table(name = "Content")
@SecondaryTable(name = "Series")
class Series(
    contentDetails: SeriesDetails? = null,
    id: Int = 0,
    title: String = "",
    year: Int = 0,
    seen: Boolean = false,
    anticipated: Boolean = false,
    rating: Int = 0,
    userId: Int = 0,
    @Column(table = "Series")
    var watching: Boolean = false,
) : Content(
    id = id,
    title = title,
    year = year,
    seen = seen,
    anticipated = anticipated,
    myRating = rating,
    contentDetails = contentDetails,
    userId = userId,
) {
    @ManyToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "contentDetailsId", updatable = false, insertable = false)
    var seriesDetails: SeriesDetails? = null

    override fun copyFrom(other: Content) {
        super.copyFrom(other)
        watching = (other as Series).watching
    }

    override fun getContentInfo() = contentDetails as? SeriesDetails

    @Transient
    override val contentType = ContentType.SERIES
}
