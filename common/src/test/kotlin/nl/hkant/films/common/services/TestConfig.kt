package nl.hkant.films.common.services

import org.springframework.context.annotation.Configuration
import org.springframework.test.context.TestPropertySource

@Configuration
// @Import(CommonConfig::class)
@TestPropertySource("classpath:test-resources.properties")
class TestConfig
