package nl.hkant.films.common.services

import nl.hkant.films.common.config.CacheConfig
import nl.hkant.films.common.testutil.BaseContextTestParent
import nl.hkant.films.external.IEpisodeInfoService
import nl.hkant.films.external.LastAndNextEpisode
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.stub
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.springframework.beans.factory.annotation.Value
import org.springframework.cache.CacheManager
import org.springframework.cache.get
import org.springframework.test.context.bean.override.mockito.MockitoBean

// @ExtendWith(SpringExtension::class)
// // @AutoConfigureDataJpa
// @DataJpaTest(
//    properties = [
//        "spring.datasource.url=jdbc:mysql://149.210.189.211:3306/films?serverTimezone=UTC&createDatabaseIfNotExist=true",
//        "spring.datasource.username=dev",
//        "spring.datasource.password=devpassword",
//    ],
// )
// @EnableAutoConfiguration
// @ContextConfiguration(classes = [TestConfig::class])
// @TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
// @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Disabled
internal class ExternalInfoCachingServiceTest(
    private val externalInfoCachingService: ExternalInfoCachingService,
    private val cacheManager: CacheManager,
    @Value("\${hibernate.dialect}")
    private val a: String,
) : BaseContextTestParent() {

//    @Test
    fun testje() {
        println()
    }

    @MockitoBean
    private lateinit var episodeInfoService: IEpisodeInfoService

    @BeforeEach
    fun init() {
        requireNotNull(cacheManager[CacheConfig.CacheNames.EPISODE_INFO]).clear()
        episodeInfoService.stub {
            on { getLastAndUpcomingEpisodes(any()) } doReturn LastAndNextEpisode(null, null)
        }
    }

//    @Test
    fun `value should be cached`() {
        repeat(4) { externalInfoCachingService.getLastAndUpcomingEpisodes(1) }
        externalInfoCachingService.getLastAndUpcomingEpisodes(1)
        externalInfoCachingService.getLastAndUpcomingEpisodes(1)
        verify(episodeInfoService, times(1)).getLastAndUpcomingEpisodes(any())
    }

//    @Test
    fun `no caching if different arguments`() {
        externalInfoCachingService.getLastAndUpcomingEpisodes(1)
        externalInfoCachingService.getLastAndUpcomingEpisodes(2)
        externalInfoCachingService.getLastAndUpcomingEpisodes(3)
        verify(episodeInfoService, times(3)).getLastAndUpcomingEpisodes(any())
    }

//    @ParameterizedTest
    @ValueSource(
        ints = [-10, 0, 321],
    )
    fun `given id is passed to external service`(value: Int) {
        externalInfoCachingService.getLastAndUpcomingEpisodes(value)
        verify(episodeInfoService).getLastAndUpcomingEpisodes(value)
    }
}
