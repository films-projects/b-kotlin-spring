rootProject.name = "films-kotlin"

include(":common")
include(":web")
include(":browser-extension")
include(":calendar-integration")
