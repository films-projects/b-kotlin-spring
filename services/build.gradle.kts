plugins {
    alias(libs.plugins.springBootDependencyManagement)
    alias(libs.plugins.kotlinSpring)
    alias(libs.plugins.kotlinJvm)
    alias(libs.plugins.kotlinter)
}

dependencies {
    implementation(project(":common"))

    implementation(libs.filmsExternalInfo)
    implementation(libs.bundles.springBootServices)
    implementation(libs.springBootCache)

    implementation(libs.bundles.caching)
}

tasks.withType<Test> {
    useJUnitPlatform()
}