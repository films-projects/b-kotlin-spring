# Films Kotlin/Spring

The kotlin version of the [Films project](https://gitlab.com/hgjkant/films-spring). This app is built using

-   Kotlin 2.0
-   Gradle 8.11
-   Spring Boot 3.4
-   target Java version 22
-   the [Films frontend](https://gitlab.com/films-projects/f-js-react)
-   the [Films 3rd party info access](https://gitlab.com/https://gitlab.com/films-projects/b-kotlin-external-infod)

## Running the application

See the [setup](docs/setup.md) for directions on running the application.
