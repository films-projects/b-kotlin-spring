plugins {
    alias(libs.plugins.springBoot)
    alias(libs.plugins.springBootDependencyManagement)
    alias(libs.plugins.kotlinSpring)
    alias(libs.plugins.kotlinJvm)
    alias(libs.plugins.kotlinter)
}

dependencies {
    implementation(project(":common"))

    implementation(libs.filmsExternalInfo)
    implementation(libs.bundles.springBootBrowserExtension)
    implementation(libs.springBootJdbc)
    implementation(libs.springBootDataJpa)
    implementation(libs.springBootActuator)
}

tasks.withType<Test> {
    useJUnitPlatform()
}