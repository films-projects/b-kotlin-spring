const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const webpack = require("webpack");

module.exports = (env, argv) => {
    const isProduction = argv.mode == "production";
    const copyPatterns = [{ from: "src/extension/static", force: true }];
    if (isProduction) {
        copyPatterns.push({ from: "src/extension/icons", force: true });
    } else {
        copyPatterns.push({ from: "src/extension/icons-dev", force: true });
    }
    return {
        devtool: "source-map",
        entry: {
            app: "./src/extension/src/app.js",
            content: "./src/extension/src/content.js",
        },
        output: {
            filename: "[name].js",
            path: path.resolve(__dirname, "dist"),
            clean: true, // Clean the output directory before emit.
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                    },
                },
                {
                    test: /\.css$/,
                    use: ["style-loader", "css-loader"],
                },
            ],
        },
        plugins: [
            new CopyWebpackPlugin({
                patterns: copyPatterns,
            }),
            new webpack.DefinePlugin({
                "process.env": JSON.stringify(env),
            }),
        ],
    };
};
