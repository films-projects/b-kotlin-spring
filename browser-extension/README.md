# kotlin-browser-extension

This project provides a microservice with endpoints to search and add content, and a browser extension which can add new
content through these endpoints. The browser extension works by, on an IMDB page, taking the IMDB id of that content, 
searching the 3rd party API (using the 
[external-info library](https://gitlab.com/films-projects/lib-kotlin-external-info)), and adding the new content. This 
project utilises

- Kotlin 2
- Gradle 8
- Spring Boot 3
- JavaScript
- React
- Prettier
- ESLint
- StyleLint
- The [external info library](https://gitlab.com/films-projects/lib-kotlin-external-info)

## Showcase

An example of the extension when on the IMDB page of the movie `Karate Kid`, and the page for the movie `Back to the 
Future`, which was already added before. Click the thumbnails for the original image.

<img src="/img/extension.png" alt="The search page" width="400"> 

<img src="/img/alreadyAdded.png" alt="The search page" width="400"> 
