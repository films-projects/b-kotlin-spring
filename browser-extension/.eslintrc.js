module.exports = {
    settings: {
        react: {
            version: "detect",
        },
    },
    env: {
        browser: true,
        node: true,
        es2021: true,
    },
    extends: ["eslint:recommended", "plugin:react/recommended"],
    overrides: [
        {
            env: {
                node: true,
            },
            files: [".eslintrc.{js,cjs}"],
            parserOptions: {
                sourceType: "script",
            },
        },
    ],
    parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
    },
    globals: {
        chrome: true,
    },
    plugins: ["react"],
    rules: {
        "react/prop-types": ["warn"],
        "react/no-deprecated": ["warn"],
        "react/display-name": ["off"],
    },
};
