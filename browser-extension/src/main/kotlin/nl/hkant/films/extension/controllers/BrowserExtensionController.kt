package nl.hkant.films.extension.controllers

import nl.hkant.films.common.dao.ConfigDao
import nl.hkant.films.common.dao.ContentDao
import nl.hkant.films.common.dao.UserDao
import nl.hkant.films.common.model.ConfigKey
import nl.hkant.films.common.services.ContentService
import nl.hkant.films.external.ContentType
import nl.hkant.films.external.IEpisodeInfoService
import nl.hkant.films.external.IExternalContentInfoFetchService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.util.HtmlUtils
import java.time.LocalDate

@RestController
class BrowserExtensionController(
    private val iExternalContentInfoFetchService: IExternalContentInfoFetchService,
    private val iEpisodeInfoService: IEpisodeInfoService,
    private val userDao: UserDao,
    private val contentDao: ContentDao,
    private val contentService: ContentService,
    private val configDao: ConfigDao,
) {

    @GetMapping("/ping")
    fun ping() = "pong (extension)"

    @GetMapping("/find/movie/{imdbId}")
    fun findMovie(@PathVariable imdbId: String, @RequestParam token: String) =
        findByImdbId(token = token, imdbId = imdbId, contentType = ContentType.FILM)

    @GetMapping("/find/series/{imdbId}")
    fun findSeries(@PathVariable imdbId: String, @RequestParam token: String) =
        findByImdbId(token = token, imdbId = imdbId, contentType = ContentType.SERIES)

    @GetMapping("/add/movie/{externalContentId}")
    fun addMovie(@PathVariable externalContentId: Int, @RequestParam token: String, @RequestParam interested: Boolean) =
        createByExternalContentId(
            token = token,
            externalContentId = externalContentId,
            contentType = ContentType.FILM,
            interested = interested,
        )

    @GetMapping("/add/series/{externalContentId}")
    fun addSeries(@PathVariable externalContentId: Int, @RequestParam token: String, @RequestParam interested: Boolean) =
        createByExternalContentId(
            token = token,
            externalContentId = externalContentId,
            contentType = ContentType.SERIES,
            interested = interested,
        )

    @GetMapping("/series")
    fun series(@RequestParam token: String): SeriesSearchResult {
        val user = userDao.findByPrivateKey(token) ?: return SeriesSearchResult.Failed("Unknown token")
        val series = contentDao.findWatchingOngoingAndUpcomingAnticipatedSeries(user)
        val episodes = series.associateWith {
            iEpisodeInfoService.getLastAndUpcomingEpisodes(it.contentDetails!!.externalContentId)
        }
        val now = LocalDate.now()
        val pastEpisodes = episodes
            .mapNotNull { it.value.lastEpisode?.let { e -> it.key to e } }
            .filter { (_, episode) -> !episode.airDate.isBefore(now.minusDays(7)) }
            .map { (series, episode) ->
                EpisodeInfo(
                    title = series.title,
                    linkToContent = getUrlToTitle(series.title),
                    season = episode.season,
                    episode = episode.episode,
                    isFinale = episode.isFinale,
                    date = episode.airDate,
                )
            }
            .sortedBy { it.date }

        val upcomingEpisodes = episodes
            .mapNotNull { it.value.nextEpisode?.let { e -> it.key to e } }
            .map { (series, episode) ->
                EpisodeInfo(
                    title = series.title,
                    linkToContent = getUrlToTitle(series.title),
                    season = episode.season,
                    episode = episode.episode,
                    isFinale = episode.isFinale,
                    date = episode.airDate,
                )
            }
            .sortedBy { it.date }

        return SeriesSearchResult.Success(pastEpisodes = pastEpisodes, upcomingEpisodes = upcomingEpisodes)
    }

    private fun findByImdbId(token: String, imdbId: String, contentType: ContentType): ContentSearchResult {
        val user = userDao.findByPrivateKey(token) ?: return ContentSearchResult.Failed("Unknown token")
        return iExternalContentInfoFetchService.findByImdbId(imdbId, contentType)
            ?.let {
                val existingContent = contentDao.findByExternalContentIdForUser(user, it.externalContentId, contentType)
                ContentSearchResult.Success(
                    posterPath = it.posterPath?.let { p ->
                        configDao.getRequired(ConfigKey.TMDB_IMAGE_BASEURL_POSTER) + p.removePrefix(
                            "/",
                        )
                    },
                    title = it.title,
                    externalContentId = it.externalContentId,
                    userHasContentAdded = existingContent != null,
                    existingContentUrl = existingContent?.let { c -> getUrlToTitle(c.title) },
                )
            }
            ?: ContentSearchResult.Failed("No content found")
    }

    private fun createByExternalContentId(
        token: String,
        externalContentId: Int,
        contentType: ContentType,
        interested: Boolean,
    ): AddResult {
        val user = userDao.findByPrivateKey(token) ?: return AddResult.Failed("Unknown token, logout and login again")
        return contentService.createFromExternalResource(
            user,
            externalContentId = externalContentId,
            type = contentType,
            interested = interested,
        )
            ?.let { AddResult.Success(newContentUrl = getUrlToTitle(it.title)) }
            ?: AddResult.Failed("No content found")
    }

    private fun getUrlToTitle(title: String): String =
        "https://kotlin.hkant.nl/search?title=" + HtmlUtils.htmlEscape(title)
}

sealed interface ContentSearchResult {
    data class Failed(val error: String) : ContentSearchResult

    data class Success(
        val posterPath: String?,
        val externalContentId: Int,
        val title: String,
        val userHasContentAdded: Boolean,
        val existingContentUrl: String?,
    ) : ContentSearchResult
}

sealed interface SeriesSearchResult {
    data class Failed(val error: String) : SeriesSearchResult

    data class Success(
        val pastEpisodes: List<EpisodeInfo>,
        val upcomingEpisodes: List<EpisodeInfo>,
    ) : SeriesSearchResult
}

data class EpisodeInfo(
    val title: String,
    val linkToContent: String,
    val season: Int,
    val episode: Int,
    val isFinale: Boolean,
    val date: LocalDate,
)

sealed interface AddResult {
    data class Failed(val error: String) : AddResult

    data class Success(val newContentUrl: String) : AddResult
}
