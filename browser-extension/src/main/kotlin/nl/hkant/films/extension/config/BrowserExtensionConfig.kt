package nl.hkant.films.extension.config

import nl.hkant.films.common.config.CommonConfig
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(CommonConfig::class)
class BrowserExtensionConfig
