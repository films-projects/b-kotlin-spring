import React from "react";
import { createRoot } from "react-dom/client";

import Login from "./views/Login";
import Content from "./views/content/Content";
import Series from "./views/series/Series";
import ErrorView from "./views/ErrorView";

import { TAB_CONTENT, TAB_SERIES } from "./components/Tabbar";

import "./app.css";

class ReactApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = { token: undefined, tab: TAB_CONTENT, loading: true };

        this.setToken = this.setToken.bind(this);
        this.setTab = this.setTab.bind(this);
    }

    componentDidMount() {
        const component = this;

        chrome.storage.sync.get("token", data => {
            component.setState({ token: data.token });
        });

        chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
            if (tabs[0]) {
                let currentUrl = tabs[0].url;

                if (currentUrl.startsWith("https://www.imdb.com/title/tt")) {
                    const imdbId = currentUrl.replace("https://www.imdb.com/title/", "").split("/")[0];

                    if (currentUrl.includes("/episodes/")) {
                        component.setState({ imdbId, isSeries: true, loading: false });
                    } else {
                        chrome.tabs
                            .sendMessage(tabs[0].id, { foo: 2 })
                            .then(response => {
                                component.setState({ imdbId, isSeries: response.isSeries, loading: false });
                            })
                            .catch(error => {
                                console.warn("Popup could not send message to tab %d", tabs[0].id, error);
                            });
                    }
                } else {
                    component.setState({ tab: TAB_SERIES, loading: false });
                }
            }
        });
    }

    setToken(token) {
        chrome.storage.sync.set({ token: token });
        this.setState({ token });
    }

    setTab(tab) {
        this.setState({ tab });
    }

    render() {
        if (!this.state.token) {
            return <Login setToken={this.setToken} />;
        } else if (this.state.loading) {
            return <ErrorView errorHead="Loading.." errorBody="" />;
        } else if (this.state.tab === TAB_CONTENT) {
            return (
                <Content
                    setTab={this.setTab}
                    setToken={this.setToken}
                    token={this.state.token}
                    imdbId={this.state.imdbId}
                    isSeries={this.state.isSeries}
                />
            );
        } else {
            return <Series setTab={this.setTab} setToken={this.setToken} token={this.state.token} />;
        }
    }
}

const app = createRoot(document.getElementById("app"));
app.render(<ReactApp />);
