import React from "react";
import PropTypes from "prop-types";

import "./ErrorView.css";
import FlexExpand from "../components/FlexExpand";
import View from "./View";

const ErrorView = ({ errorHead, errorBody, children }) => (
    <View>
        <div id="view-error">
            <div id="error">
                <FlexExpand />
                <div id="error-header">{errorHead}</div>
                <div id="error-body">{errorBody}</div>
                <FlexExpand />
                {children}
            </div>
        </div>
    </View>
);

ErrorView.propTypes = {
    errorHead: PropTypes.string.isRequired,
    errorBody: PropTypes.string.isRequired,
    children: PropTypes.any,
};

export default ErrorView;
