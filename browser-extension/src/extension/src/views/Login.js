import React, { useState } from "react";
import PropTypes from "prop-types";

import View from "./View";

import "./Login.css";
import FlexExpand from "../components/FlexExpand";

const Login = ({ setToken }) => {
    const [input, setInput] = useState("");
    return (
        <View className="view-login">
            <div id="login-form">
                <FlexExpand />
                <input type="text" value={input} onChange={e => setInput(e.target.value)} placeholder="User token" />
                <button onClick={() => setToken(input)}>Save token</button>
                <FlexExpand />
            </div>
        </View>
    );
};

Login.propTypes = {
    setToken: PropTypes.func.isRequired,
};

export default Login;
