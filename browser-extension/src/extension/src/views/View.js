import React from "react";
import PropTypes from "prop-types";

import "./View.css";

const View = ({ className, children }) => (
    <div id="view" className={className}>
        {children}
    </div>
);

View.propTypes = {
    className: PropTypes.string,
    children: PropTypes.any.isRequired,
};

export default View;
