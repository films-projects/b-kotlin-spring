import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";
import moment from "moment";

import "./UpcomingEpisodes.css";

function getRelativeDays(date) {
    const momentObj = moment(date, "YYYY-MM-DD");
    const now = moment().startOf("day");
    const difference = moment.duration(now.diff(momentObj)).asDays();
    const days = Math.round(Math.abs(difference));
    const singularOrPlural = days === 1 ? "day" : "days";
    if (difference === 0) {
        return "Today";
    }
    return difference > 0 ? `${days} ${singularOrPlural} ago` : `in ${days} ${singularOrPlural}`;
}

const EpisodeNumber = ({ episode }) => {
    let seasonAndEpisode = episode.episode + "";
    if (seasonAndEpisode.length < 2) {
        seasonAndEpisode = seasonAndEpisode.padStart(2, "0");
    }
    seasonAndEpisode = "E" + seasonAndEpisode;
    seasonAndEpisode = episode.season + seasonAndEpisode;
    if (seasonAndEpisode.length < 5) {
        seasonAndEpisode = seasonAndEpisode.padStart(5, "0");
    }
    seasonAndEpisode = "S" + seasonAndEpisode;
    return (
        <span className={cls("episodeNumber", { finale: episode.isFinale, start: episode.episode === 1 })}>
            {seasonAndEpisode}
        </span>
    );
};

EpisodeNumber.propTypes = {
    episode: PropTypes.object.isRequired,
};

const UpcomingEpisodes = ({ series }) => (
    <div id="episodes">
        <div className="episodes-container">
            <div className="episodes-container-header">Past episodes</div>
            {series.pastEpisodes.map(s => {
                return (
                    <a href={s.linkToContent} target="_blank" key={s.title} rel="noreferrer">
                        <div className="episode">
                            <span className="title">{s.title}</span>
                            <span className="date">{getRelativeDays(s.date)}</span>
                            <EpisodeNumber episode={s} />
                        </div>
                    </a>
                );
            })}
        </div>
        <div className="episodes-container">
            <div className="episodes-container-header">Upcoming episodes</div>
            {series.upcomingEpisodes.map(s => {
                return (
                    <a href={s.linkToContent} target="_blank" key={s.title} rel="noreferrer">
                        <div className="episode">
                            <span className="title">{s.title}</span>
                            <span className="date">{getRelativeDays(s.date)}</span>
                            <EpisodeNumber episode={s} />
                        </div>
                    </a>
                );
            })}
        </div>
    </div>
);

UpcomingEpisodes.propTypes = {
    series: PropTypes.array.isRequired,
};

export default UpcomingEpisodes;
