import React from "react";
import PropTypes from "prop-types";

import View from "../View";
import LogoutButton from "../../components/LogoutButton";

import FlexExpand from "../../components/FlexExpand";
import ErrorView from "../ErrorView";
import { doRequest } from "../../util/doRequest";
import Tabbar, { TAB_SERIES } from "../../components/Tabbar";
import UpcomingEpisodes from "./UpcomingEpisodes";

class Series extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            series: undefined,
            loading: true,
        };
    }

    componentDidMount() {
        doRequest(`/series?token=${this.props.token}`)
            .then(r => r.json())
            .then(a => this.setState({ series: a, loading: false }));
    }

    render() {
        let contentView;
        if (this.state.loading) {
            contentView = <ErrorView errorHead="Loading.." errorBody="" />;
        } else if (this.state.error) {
            contentView = <ErrorView errorHead="Failed to show series" errorBody={this.state.error} />;
        } else if (this.state.series) {
            contentView = <UpcomingEpisodes series={this.state.series} />;
        } else {
            contentView = <ErrorView errorHead="Failed to make a request" errorBody="Is the application running?" />;
        }
        return (
            <View className="background-yellow">
                <Tabbar setTab={this.props.setTab} currentTab={TAB_SERIES} />
                {contentView}
                <FlexExpand />
                <LogoutButton setToken={this.props.setToken} />
            </View>
        );
    }
}

Series.propTypes = {
    token: PropTypes.string,
    setTab: PropTypes.func.isRequired,
    setToken: PropTypes.func.isRequired,
};

export default Series;
