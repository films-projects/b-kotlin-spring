import React from "react";
import PropTypes from "prop-types";

import "./ContentAddedView.css";
import FlexExpand from "../../components/FlexExpand";

const ContentAddedView = ({ posterPath, title, newContentUrl }) => (
    <div id="content-added">
        <img id="poster" src={posterPath} />
        <div id="info">
            <div id="title">{title}</div>
            <div id="info-line-1">Content was added!</div>
            <div id="info-line-2">You can view it via the link below</div>
            <FlexExpand />
            <a target="_blank" href={newContentUrl} rel="noreferrer">
                View new content
            </a>
        </div>
    </div>
);

ContentAddedView.propTypes = {
    posterPath: PropTypes.string,
    title: PropTypes.string.isRequired,
    newContentUrl: PropTypes.string.isRequired,
};

export default ContentAddedView;
