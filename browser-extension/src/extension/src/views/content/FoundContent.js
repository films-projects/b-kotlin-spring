import React from "react";
import PropTypes from "prop-types";

import "./FoundContent.css";
import FlexExpand from "../../components/FlexExpand";

class FoundContent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { posterPath, title, userHasContentAdded, addContent, existingContentUrl } = this.props;
        let alreadyAdded;
        let buttons;
        if (userHasContentAdded) {
            alreadyAdded = (
                <>
                    <div>You have already added this content</div>
                    <FlexExpand />
                    <a target="_blank" href={existingContentUrl} rel="noreferrer">
                        View this content
                    </a>
                </>
            );
        } else {
            buttons = (
                <>
                    <FlexExpand />
                    <div>
                        <button className="btn-add-content" onClick={() => addContent(false)}>
                            Add content
                        </button>
                        <button className="btn-add-content anticipated" onClick={() => addContent(true)}>
                            <span>Add with</span>
                            <img src="anticipated.svg" />
                        </button>
                    </div>
                </>
            );
        }
        return (
            <div id="found-content">
                <img id="poster" src={posterPath} />
                <div id="info">
                    <div id="title">{title}</div>
                    {alreadyAdded}
                    {buttons}
                </div>
            </div>
        );
    }
}

FoundContent.propTypes = {
    posterPath: PropTypes.string,
    existingContentUrl: PropTypes.func,
    title: PropTypes.string.isRequired,
    userHasContentAdded: PropTypes.bool.isRequired,
    addContent: PropTypes.func.isRequired,
};

export default FoundContent;
