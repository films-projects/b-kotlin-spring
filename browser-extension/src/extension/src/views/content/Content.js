import React from "react";
import PropTypes from "prop-types";

import View from "../View";
import LogoutButton from "../../components/LogoutButton";

import FoundContent from "./FoundContent";
import ContentAddedView from "./ContentAddedView";
import FlexExpand from "../../components/FlexExpand";
import ErrorView from "../ErrorView";
import { doRequest } from "../../util/doRequest";
import Tabbar, { TAB_CONTENT } from "../../components/Tabbar";

class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            content: undefined,
            contentAdded: false,
            urlToNewContent: undefined,
            addFailed: false,
            loading: true,
        };

        this.addContent = this.addContent.bind(this);
    }

    addContent(interested) {
        const { token } = this.props;
        const { content, isSeries } = this.state;
        const { externalContentId } = content;

        const type = isSeries ? "series" : "movie";
        doRequest(`/add/${type}/${externalContentId}?token=${token}&interested=${interested}`)
            .then(res => res.json())
            .then(data => this.setState(data));
    }

    componentDidMount() {
        const { imdbId, isSeries, token } = this.props;
        const type = isSeries ? "series" : "movie";

        doRequest(`/find/${type}/${imdbId}?token=${token}`)
            .then(r => r.json())
            .then(a => this.setState({ content: a, loading: false }));
    }

    render() {
        let contentView;
        if (!this.props.imdbId) {
            contentView = (
                <ErrorView
                    errorHead="You cannot use the extension on this page"
                    errorBody="Please go to an IMDB movie or series to use this extension"
                />
            );
        } else if (this.state.loading) {
            contentView = <ErrorView errorHead="Loading.." errorBody="" />;
        } else if (this.state.error) {
            contentView = <ErrorView errorHead="Failed to add content" errorBody={this.state.error} />;
        } else if (this.state.content && this.state.content.error) {
            contentView = <ErrorView errorHead="Failed to add content" errorBody={this.state.content.error} />;
        } else if (this.state.newContentUrl) {
            const { title, posterPath } = this.state.content;
            contentView = (
                <ContentAddedView posterPath={posterPath} title={title} newContentUrl={this.state.newContentUrl} />
            );
        } else if (this.state.content) {
            const { externalContentId, title, userHasContentAdded, posterPath, existingContentUrl } =
                this.state.content;
            contentView = (
                <FoundContent
                    externalContentId={externalContentId}
                    posterPath={posterPath}
                    title={title}
                    token={this.props.token}
                    isSeries={this.props.isSeries}
                    addContent={this.addContent}
                    existingContentUrl={existingContentUrl}
                    userHasContentAdded={userHasContentAdded}
                />
            );
        } else {
            contentView = <ErrorView errorHead="Failed to make a request" errorBody="Is the application running?" />;
        }
        return (
            <View className="background-yellow">
                <Tabbar setTab={this.props.setTab} currentTab={TAB_CONTENT} />
                {contentView}
                <FlexExpand />
                <LogoutButton setToken={this.props.setToken} />
            </View>
        );
    }
}

Content.propTypes = {
    token: PropTypes.string,
    isSeries: PropTypes.bool,
    imdbId: PropTypes.string,
    setToken: PropTypes.func.isRequired,
    setTab: PropTypes.func.isRequired,
};

Content.defaultProps = {
    isSeries: false,
};

export default Content;
