import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

export const TAB_CONTENT = "Content";
export const TAB_SERIES = "Series";

import "./Tabbar.css";

const Tabbar = ({ setTab, currentTab }) => (
    <div id="tabbar">
        <div className={cls("tab", { selected: currentTab === TAB_CONTENT })} onClick={() => setTab(TAB_CONTENT)}>
            {TAB_CONTENT}
        </div>
        <div className={cls("tab", { selected: currentTab === TAB_SERIES })} onClick={() => setTab(TAB_SERIES)}>
            {TAB_SERIES}
        </div>
    </div>
);

Tabbar.propTypes = {
    setTab: PropTypes.func.isRequired,
    currentTab: PropTypes.string.isRequired,
};

export default Tabbar;
