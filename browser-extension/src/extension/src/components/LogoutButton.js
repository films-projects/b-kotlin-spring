import React from "react";
import PropTypes from "prop-types";

import "./LogoutButton.css";

const LogoutButton = ({ setToken }) => (
    <a onClick={() => setToken(null)} href="#" id="logout-button">
        Logout
    </a>
);

LogoutButton.propTypes = {
    setToken: PropTypes.func.isRequired,
};

export default LogoutButton;
