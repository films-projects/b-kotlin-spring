group = "nl.hkant.films"

plugins {
    alias(libs.plugins.springBootDependencyManagement)
    alias(libs.plugins.kotlinSpring)
    alias(libs.plugins.kotlinJvm)
    alias(libs.plugins.kotlinKapt)
    alias(libs.plugins.kotlinter)
}

allprojects {
    version = "2.13"

    apply {
        plugin("java")
    }

    java {
        sourceCompatibility = JavaVersion.VERSION_23
        targetCompatibility = JavaVersion.VERSION_23
    }

    repositories {
        mavenCentral()

        // external info library
        maven {
            name = "GitLab"

            url = uri("https://gitlab.com/api/v4/projects/64664622/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = properties["filmsPrivateToken"] as String?
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}