plugins {
    alias(libs.plugins.springBoot)
    alias(libs.plugins.springBootDependencyManagement)
    alias(libs.plugins.kotlinSpring)
    alias(libs.plugins.kotlinJvm)
    alias(libs.plugins.kotlinKapt)
    alias(libs.plugins.kotlinter)
}

dependencies {
    implementation(project(":common"))

    implementation(libs.ical4j)
    implementation(libs.filmsExternalInfo)

    implementation(libs.bundles.springBootCalendarIntegration)
    implementation(libs.springBootActuator)

    testImplementation(libs.bundles.testing)
}

tasks.withType<Test> {
    useJUnitPlatform()
}