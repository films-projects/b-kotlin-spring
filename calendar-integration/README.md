# `Films` calendar integration

This is a small application to expose an `.ics` calendar which a user can subscribe to. This calendar includes the dates
for upcoming movies and episodes for select series. This project utilises

- Kotlin 2
- Gradle 8
- Spring boot 3
- The [external info library](https://gitlab.com/films-projects/lib-kotlin-external-info)

## Showcase

An example of a calendar subscribed to the provided `.ics` calendar. Click the thumbnail for the original image.

<img src="/img/calendar.png" alt="The search page" width="500"> 