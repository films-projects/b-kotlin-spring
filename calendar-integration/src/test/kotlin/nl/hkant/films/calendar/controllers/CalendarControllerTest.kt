package nl.hkant.films.calendar.controllers

import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get

internal class CalendarControllerTest(
    private val mockMvc: MockMvc,
) {

//    @Test
    fun ping() {
        mockMvc
            .get("/ping")
            .andExpect {
                status { isOk() }
                content {
                    string("pong")
                }
            }
    }
}
