package nl.hkant.films.calendar.dto

import java.time.LocalDate

sealed class CalendarEvent(
    val contentTitle: String,
    val date: LocalDate,
) {
    class FilmEvent(
        contentTitle: String,
        date: LocalDate,
    ) : CalendarEvent(contentTitle, date)

    class SeriesEvent(
        contentTitle: String,
        date: LocalDate,
        val season: Int,
        val episode: Int,
        val isFinale: Boolean,
    ) : CalendarEvent(contentTitle, date)
}
