package nl.hkant.films.calendar.controllers

import jakarta.servlet.http.HttpServletResponse
import nl.hkant.films.calendar.dto.CalendarEvent
import nl.hkant.films.calendar.dto.FilmsCalendar
import nl.hkant.films.common.dao.ContentDao
import nl.hkant.films.common.dao.UserDao
import nl.hkant.films.common.model.User
import nl.hkant.films.common.services.ExternalInfoCachingService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import java.time.LocalDate

@Controller
class CalendarController(
    private val contentDao: ContentDao,
    private val userDao: UserDao,
    private val externalInfoCachingService: ExternalInfoCachingService,
) {
    @GetMapping("/ping")
    @ResponseBody
    fun ping(): String {
        return "pong (calendar)"
    }

    @GetMapping("/calendar")
    fun calendar(
        @RequestParam privateKey: String,
        httpServletResponse: HttpServletResponse,
    ) {
        val user = userDao.findByPrivateKey(privateKey) ?: run {
            httpServletResponse.status = HttpStatus.NOT_FOUND.value()
            return
        }
        val filmEvents = getFilmEvents(user)
        val seriesEvents = getSeriesEvents(user)
        val calendar = FilmsCalendar()
        calendar.addEvents(filmEvents)
        calendar.addEvents(seriesEvents)
        calendar.outputToStream(httpServletResponse.outputStream)
    }

    private fun getFilmEvents(user: User) =
        contentDao.getUpcomingFilmsInUpcomingMonth(user, LocalDate.now())
            .map {
                CalendarEvent.FilmEvent(
                    it.title,
                    it.movieDetails!!.airDate!!,
                )
            }

    private fun getSeriesEvents(user: User) =
        contentDao.getWatchingOrUpcomingSeries(user)
            .map {
                Pair(
                    it,
                    externalInfoCachingService.getLastAndUpcomingEpisodes(it.contentDetails!!.externalContentId).nextEpisode,
                )
            }
            .mapNotNull { (series, episode) -> episode?.let { Pair(series, episode) } }
            .map { (series, episode) ->
                CalendarEvent.SeriesEvent(
                    contentTitle = series.title,
                    date = episode.airDate,
                    season = episode.season,
                    episode = episode.episode,
                    isFinale = episode.isFinale,
                )
            }

    @GetMapping("/test")
    fun test(httpServletResponse: HttpServletResponse) {
        val calendar = FilmsCalendar()
        calendar.createTestAlarmEvents()
        calendar.outputToStream(httpServletResponse.outputStream)
    }
}
