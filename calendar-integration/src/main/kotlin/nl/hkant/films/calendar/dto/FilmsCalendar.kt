package nl.hkant.films.calendar.dto

import net.fortuna.ical4j.data.CalendarOutputter
import net.fortuna.ical4j.model.Calendar
import net.fortuna.ical4j.model.ComponentContainer
import net.fortuna.ical4j.model.Dur
import net.fortuna.ical4j.model.PropertyContainer
import net.fortuna.ical4j.model.PropertyList
import net.fortuna.ical4j.model.component.CalendarComponent
import net.fortuna.ical4j.model.component.VAlarm
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.property.Description
import net.fortuna.ical4j.model.property.ProdId
import net.fortuna.ical4j.model.property.Trigger
import net.fortuna.ical4j.model.property.immutable.ImmutableAction
import net.fortuna.ical4j.model.property.immutable.ImmutableCalScale
import net.fortuna.ical4j.model.property.immutable.ImmutableVersion
import net.fortuna.ical4j.util.FixedUidGenerator
import java.io.OutputStream
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.ChronoUnit

class FilmsCalendar {
    private val instance =
        Calendar()
            .also {
                it.add<PropertyContainer>(ProdId("-//Films//iCal4j 1.0//EN"))
                it.add<PropertyContainer>(ImmutableVersion.VERSION_2_0)
                it.add<PropertyContainer>(ImmutableCalScale.GREGORIAN)
            }

    fun addEvents(events: List<CalendarEvent>) {
        for (calendarEvent in events) {
            val newEvent = when (calendarEvent) {
                is CalendarEvent.FilmEvent -> createFilmEvent(calendarEvent)
                is CalendarEvent.SeriesEvent -> createSeriesEvent(calendarEvent)
            }
            instance.add<ComponentContainer<CalendarComponent>>(newEvent)
        }
    }

    fun outputToStream(out: OutputStream) {
        CalendarOutputter().output(instance, out)
    }

    private fun createFilmEvent(event: CalendarEvent.FilmEvent): VEvent {
        val uid = event.contentTitle + "." + event.date.toString()
        val ug = FixedUidGenerator(uid)
        return VEvent(event.date, event.contentTitle).also {
            it.add<PropertyContainer>(ug.generateUid())
            it.add<VAlarm>(createAlarm(event))
        }
    }

    private fun createSeriesEvent(event: CalendarEvent.SeriesEvent): VEvent {
        val title = if (event.isFinale) {
            "${event.getTitle()} (Finale)"
        } else {
            event.getTitle()
        }

        val uid: String = event.contentTitle + "." + event.season + "." + event.episode
        val ug = FixedUidGenerator(uid)
        return VEvent(event.date, title).also {
            it.add<PropertyContainer>(ug.generateUid())
            it.add<VAlarm>(createAlarm(event))
        }
    }

    private fun createAlarm(event: CalendarEvent): VAlarm {
        val name = when (event) {
            is CalendarEvent.FilmEvent -> "${event.contentTitle} was released!"
            is CalendarEvent.SeriesEvent -> "${event.getTitle()} aired!"
        }

        val alarmInstant = event.date.atStartOfDay(ZoneId.systemDefault()).toInstant().plus(6, ChronoUnit.HOURS)
        return VAlarm(
            PropertyList(
                listOf(
                    ImmutableAction.DISPLAY,
                    Description(name),
                    Trigger(alarmInstant),
                ),
            ),
        )
    }

    companion object {
        fun CalendarEvent.SeriesEvent.getTitle(): String {
            return "$contentTitle (S${season}E$episode)"
        }
    }

    fun createTestAlarmEvents() {
        val tomorrow = LocalDate.now().plusDays(1).atStartOfDay(ZoneId.systemDefault())
        val theDayAfterTomorrow = LocalDate.now().plusDays(2).atStartOfDay(ZoneId.systemDefault())

        listOf(tomorrow, theDayAfterTomorrow).forEach { date ->
            val instant = date.toInstant()

            val tomorrowRelativePastAlarm = VEvent(date, "$instant-rel-past").also {
                val ug = FixedUidGenerator("$instant-rel-past")
                it.add<PropertyContainer>(ug.generateUid())

                val alarm = VAlarm(
                    PropertyList(
                        listOf(
                            ImmutableAction.DISPLAY,
                            Description("$instant-rel-past"),
                            Trigger(Dur("-PT15M")),
                        ),
                    ),
                )
                it.add<VAlarm>(alarm)
            }

            val tomorrowRelativeFutureAlarm = VEvent(date, "$instant-rel-future").also {
                val ug = FixedUidGenerator("$instant-rel-future")
                it.add<PropertyContainer>(ug.generateUid())

                val alarm = VAlarm(
                    PropertyList(
                        listOf(
                            ImmutableAction.DISPLAY,
                            Description("$instant-rel-future"),
                            Trigger(Dur("+PT15M")),
                        ),
                    ),
                )
                it.add<VAlarm>(alarm)
            }

            val tomorrowAbsolutePastAlarm = VEvent(date, "$instant-abs-past").also {
                val ug = FixedUidGenerator("$instant-abs-past")
                it.add<PropertyContainer>(ug.generateUid())

                val alarmInstant = date.toInstant().minus(1, ChronoUnit.HOURS)
                val alarm = VAlarm(
                    PropertyList(
                        listOf(
                            ImmutableAction.DISPLAY,
                            Description("$instant-abs-past"),
                            Trigger(alarmInstant),
                        ),
                    ),
                )
                it.add<VAlarm>(alarm)
            }

            val tomorrowAbsoluteFutureAlarm = VEvent(date, "$instant-abs-future").also {
                val ug = FixedUidGenerator("$instant-abs-future")
                it.add<PropertyContainer>(ug.generateUid())

                val alarmInstant = date.toInstant().plus(1, ChronoUnit.HOURS)
                val alarm = VAlarm(
                    PropertyList(
                        listOf(
                            ImmutableAction.DISPLAY,
                            Description("$instant-abs-future"),
                            Trigger(alarmInstant),
                        ),
                    ),
                )
                it.add<VAlarm>(alarm)
            }
            instance.add<ComponentContainer<CalendarComponent>>(tomorrowRelativePastAlarm)
            instance.add<ComponentContainer<CalendarComponent>>(tomorrowRelativeFutureAlarm)
            instance.add<ComponentContainer<CalendarComponent>>(tomorrowAbsolutePastAlarm)
            instance.add<ComponentContainer<CalendarComponent>>(tomorrowAbsoluteFutureAlarm)
        }
    }
}
