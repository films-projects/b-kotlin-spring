package nl.hkant.films.calendar.config

import nl.hkant.films.common.config.CommonConfig
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(CommonConfig::class)
class CalendarConfig
