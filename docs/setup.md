# Setup

TODO

# Listing endpoints

To check conformity with the frontend, you can list all registered endpoints in the application. All endpoints used by
the frontend are lister [here](../frontend/docs/endpoints.md). To list the application endpoints, enable the `listendpoints`
profile

# Linting

You can use the `kotlinter` plugin to ensure all code adheres to the code style. Use

```
gradlew lintKotlin
```

to check for errors, or

```
gradlew formatKotlin
```

to auto-correct errors if possible.

## Pre-commit hook

You can add `kotlinter` as a pre-commit hook, so that the code is checked before making a commit. The
[`kotlinter.pre-commit`](./kotlinter.pre-commit) file contains an example of this. Copy this file to the `.git/hooks` dir in the root dir of
this project. `.git` is a hidden folder, so you'll have to manually open it to see it. Rename the file to `pre-commit`
(without an extension) so that it can be picked up by git. If that file already exists, combine the example file with
that file.
