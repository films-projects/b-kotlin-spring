plugins {
    alias(libs.plugins.springBoot)
    alias(libs.plugins.springBootDependencyManagement)
    alias(libs.plugins.kotlinSpring)
    alias(libs.plugins.kotlinJvm)
    alias(libs.plugins.kotlinter)
}

dependencies {
    implementation(project(":common"))

    implementation(libs.filmsExternalInfo)
    implementation(libs.bundles.springBootWeb)
    implementation(libs.springBootCache)
    implementation(libs.springBootDevtools)
    implementation(libs.springBootActuator)

    implementation(libs.bundles.flyway)
    implementation(libs.jacksonKotlin)
    implementation(libs.kotlinReflect)
    implementation(libs.kotlinLibJdk8)
}

tasks.withType<Test> {
    useJUnitPlatform()
}