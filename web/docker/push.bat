@echo off
ECHO Logging in to registry..
docker login registry.gitlab.com/films-projects/b-kotlin-spring

SET /p tag="Give docker tag: "

ECHO Building docker image
docker build -f Dockerfile -t registry.gitlab.com/films-projects/b-kotlin-spring/web ../

ECHO Tagging..
docker tag registry.gitlab.com/films-projects/b-kotlin-spring registry.gitlab.com/films-projects/b-kotlin-spring/web:%tag%

ECHO Pushing tag..
docker push registry.gitlab.com/films-projects/b-kotlin-spring/web:%tag%

ECHO Pushing latest..
docker push registry.gitlab.com/films-projects/b-kotlin-spring/web

