
CREATE DATABASE  IF NOT EXISTS `films` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `films`;

CREATE TABLE `TmdbMovie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `backdropPath` varchar(255) DEFAULT NULL,
  `imdbId` varchar(15) DEFAULT NULL,
  `overview` longtext,
  `posterPath` varchar(255) DEFAULT NULL,
  `releaseDate` varchar(255) DEFAULT NULL,
  `retrieveDate` datetime DEFAULT NULL,
  `runtime` int(11) NOT NULL,
  `rating` varchar(5) DEFAULT NULL,
  `tagline` varchar(255) DEFAULT NULL,
  `tmdbId` int(11) NOT NULL,
  `film_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=latin1;


CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jreodf78a7pl5qidfh43axdfb` (`username`),
  UNIQUE KEY `UK_e6gkqunxajvyxl5uctpl2vl2p` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


CREATE TABLE `UserRole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKpw5ucx9wbc0ttyjhidr8xwbg2` (`role`,`userId`),
  KEY `FK9e1tcga17su515dcucrlj3vtj` (`userId`),
  CONSTRAINT `FK9e1tcga17su515dcucrlj3vtj` FOREIGN KEY (`userId`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;


CREATE TABLE `Film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bought` datetime DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `runtime` int(11) NOT NULL,
  `seen` bit(1) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `want` varchar(20) NOT NULL,
  `year` int(11) NOT NULL,
  `tagline` varchar(255) DEFAULT NULL,
  `plot` varchar(2000) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=884 DEFAULT CHARSET=latin1;


CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `User` (`id`,`email`,`enabled`,`password`,`username`) VALUES (1,NULL,1,'$2a$11$473.Jz3z851.fBLmv5hFL.B4lujsBT4OwM6ysXApPeJdctcKK6o/C','henk');