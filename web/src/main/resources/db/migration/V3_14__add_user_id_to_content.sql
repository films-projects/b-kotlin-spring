ALTER TABLE `films`.`Content`
    ADD COLUMN `userId` INT NULL;

UPDATE films.Content set userId = (SELECT id FROM films.User where username = 'henk');

ALTER TABLE `films`.`Content`
    CHANGE COLUMN `userId` `userId` INT NOT NULL ;

ALTER TABLE `films`.`Content`
    ADD CONSTRAINT `fk_userid_user`
        FOREIGN KEY (`userId`)
            REFERENCES `films`.`User` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;
