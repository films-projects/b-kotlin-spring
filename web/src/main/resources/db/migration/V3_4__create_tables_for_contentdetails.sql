ALTER TABLE `films`.`Series`
    DROP FOREIGN KEY `fk_id_content`;

ALTER TABLE `films`.`Content`
    CHANGE COLUMN `id` `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `films`.`Series`
    CHANGE COLUMN `id` `id` MEDIUMINT UNSIGNED NOT NULL;

ALTER TABLE `films`.`Series`
    ADD CONSTRAINT `fk_id_content`
        FOREIGN KEY (`id`)
            REFERENCES `films`.`Content` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION;

CREATE TABLE `films`.`ContentDetails`
(
    `content_id`   MEDIUMINT UNSIGNED NOT NULL,
    `type`         VARCHAR(10)        NOT NULL,
    `backdropPath` VARCHAR(255)       NULL DEFAULT NULL,
    `posterPath`   VARCHAR(255)       NULL DEFAULT NULL,
    `imdbId`       VARCHAR(15)        NULL DEFAULT NULL,
    `overview`     LONGTEXT           NULL DEFAULT NULL,
    `lastRefresh`  DATE               NULL DEFAULT NULL,
    `rating`       DOUBLE             NULL DEFAULT NULL,
    `tmdbId`       INT                NOT NULL,
    `imdbRating`   DOUBLE             NULL DEFAULT NULL,
    PRIMARY KEY (`content_id`),
    INDEX `fk_content_id_content_idx` (`content_id` ASC) VISIBLE,
    CONSTRAINT `fk_content_id_content`
        FOREIGN KEY (`content_id`)
            REFERENCES `films`.`Content` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE `films`.`MovieDetails`
(
    `content_id`  MEDIUMINT UNSIGNED NOT NULL,
    `runtime`     SMALLINT UNSIGNED  NOT NULL DEFAULT 0,
    `releaseDate` VARCHAR(15)        NULL     DEFAULT NULL,
    PRIMARY KEY (`content_id`),
    CONSTRAINT `fk_movie_content_id_contentdetails`
        FOREIGN KEY (`content_id`)
            REFERENCES `films`.`ContentDetails` (`content_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4;


CREATE TABLE `films`.`SeriesDetails`
(
    `content_id`     MEDIUMINT UNSIGNED NOT NULL,
    `episodeRuntime` SMALLINT UNSIGNED  NOT NULL,
    `firstAirDate`   VARCHAR(15)        NULL DEFAULT NULL,
    `lastAirDate`    VARCHAR(15)        NULL DEFAULT NULL,
    `numEpisodes`    SMALLINT UNSIGNED  NOT NULL,
    `numSeasons`     TINYINT UNSIGNED   NOT NULL,
    `status`         VARCHAR(25)        NULL DEFAULT NULL,
    PRIMARY KEY (`content_id`),
    CONSTRAINT `fk_content_id_contentdetails`
        FOREIGN KEY (`content_id`)
            REFERENCES `films`.`ContentDetails` (`content_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4;

-- prepare and copy data to new tables
DELETE
from films.TmdbMovie
where film_id is null;
DELETE
from films.TmdbSeries
where series_id is null;

INSERT INTO films.ContentDetails
    (SELECT film_id,
            "FILM",
            null,
            posterPath,
            imdbId,
            overview,
            lastRefresh,
            rating,
            tmdbId,
            imdbRating
     from films.TmdbMovie);

INSERT INTO films.MovieDetails (SELECT film_id, runtime, releaseDate from films.TmdbMovie);

INSERT INTO films.ContentDetails
    (SELECT series_id,
            "SERIES",
            null,
            posterPath,
            imdbId,
            overview,
            lastRefresh,
            rating,
            tmdbId,
            imdbRating
     from films.TmdbSeries);

INSERT INTO films.SeriesDetails
    (SELECT series_id,
            episodeRuntime,
            firstAirDate,
            lastAirDate,
            numEpisodes,
            numSeasons,
            status
     from films.TmdbSeries);
