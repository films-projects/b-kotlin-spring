ALTER TABLE `films`.`Film`
    DROP COLUMN `rating`,
    DROP COLUMN `plot`,
    DROP COLUMN `tagline`,
    DROP COLUMN `runtime`;
