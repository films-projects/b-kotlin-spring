ALTER TABLE `films`.`TmdbMovie`
    ADD COLUMN `imdbRating` VARCHAR(45) NULL AFTER `film_id`;

INSERT INTO `films`.`Configuration` (`configKey`, `configValue`) VALUES ('omdb.token', '');
