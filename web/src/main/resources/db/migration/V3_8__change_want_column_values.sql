UPDATE films.Content set want = 0 where want = 'UNKNOWN';
UPDATE films.Content set want = 1 where want = 'NO';
UPDATE films.Content set want = 3 where want = 'MAYBE';
UPDATE films.Content set want = 5 where want = 'YES';

ALTER TABLE `films`.`Content`
    CHANGE COLUMN `want` `rating` TINYINT UNSIGNED NOT NULL DEFAULT 0 ;
