ALTER TABLE `films`.`Content`
    ADD COLUMN `anticipated` BIT(1) NOT NULL DEFAULT false AFTER `year`;
