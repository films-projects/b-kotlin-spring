UPDATE `films`.`SeriesDetails` set status = 'ONGOING' where status = 'Returning Series';
UPDATE `films`.`SeriesDetails` set status = 'ENDED' where status = 'Ended';
UPDATE `films`.`SeriesDetails` set status = 'CANCELLED' where status = 'Canceled';
UPDATE `films`.`SeriesDetails` set status = 'UPCOMING' where status = 'Planned';
UPDATE `films`.`SeriesDetails` set status = 'UPCOMING' where status = 'In Production';
