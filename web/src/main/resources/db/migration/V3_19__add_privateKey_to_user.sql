ALTER TABLE `films`.`User`
ADD COLUMN `privateKey` VARCHAR(32) NOT NULL DEFAULT "";

UPDATE films.User set privateKey = MD5(RAND());

ALTER TABLE `films`.`User`
ADD UNIQUE INDEX `privateKey_UNIQUE` (`privateKey` ASC) VISIBLE;