CREATE TABLE `films`.`SearchFavorite` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `query` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),

  CONSTRAINT `fk_favorite_userid_user`
    FOREIGN KEY (`userId`)
    REFERENCES `films`.`User` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
);

ALTER TABLE `films`.`SearchFavorite`
ADD UNIQUE INDEX `unique_userId_query` (`userId` ASC, `query` ASC) VISIBLE ;
