DELETE FROM `films`.`Configuration` WHERE (`configKey` = 'bot.command.prefix');
DELETE FROM `films`.`Configuration` WHERE (`configKey` = 'bot.discord.token');
DELETE FROM `films`.`Configuration` WHERE (`configKey` = 'bot.user.id');
INSERT INTO `films`.`Configuration` (`configKey`, `configValue`) VALUES ('watch.options.usable', 'NETFLIX,AMAZON_PRIME,SKY_SHOWTIME');
