CREATE TABLE `films`.`Series`
(
    `id`       INT    NOT NULL,
    `watching` BIT(1) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_id_content`
        FOREIGN KEY (`id`)
            REFERENCES `films`.`Film` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;

insert into films.Series (select id, watching from films.Film where type = 'series');

ALTER TABLE `films`.`Film`
    DROP COLUMN `watching`;

ALTER TABLE `films`.`Film`
    CHANGE COLUMN `type` `type` VARCHAR(10) NOT NULL , RENAME TO  `films`.`Content` ;
