INSERT INTO `films`.`Configuration` (`configKey`, `configValue`) VALUES ('tmdb.image.baseurl.backdrop', 'https://image.tmdb.org/t/p/original/');
INSERT INTO `films`.`Configuration` (`configKey`, `configValue`) VALUES ('tmdb.image.baseurl.poster', 'https://image.tmdb.org/t/p/w300/');
