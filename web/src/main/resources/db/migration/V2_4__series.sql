ALTER TABLE `films`.`Film` ADD COLUMN `type` VARCHAR(10) NOT NULL DEFAULT 'FILM' AFTER id;

Update `films`.`Film` SET `type` = "FILM";

CREATE TABLE `films`.`TmdbSeries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `backdropPath` varchar(255) DEFAULT NULL,
  `imdbId` varchar(15) DEFAULT NULL,
  `imdbRating` varchar(255) DEFAULT NULL,
  `overview` varchar(2000) DEFAULT NULL,
  `posterPath` varchar(255) DEFAULT NULL,
  `rating` varchar(5) DEFAULT NULL,
  `tmdbId` int(11) NOT NULL,
  `episodeRuntime` int(11) NOT NULL,
  `firstAirDate` varchar(255) DEFAULT NULL,
  `inProgress` bit(1) NOT NULL,
  `lastAirDate` varchar(255) DEFAULT NULL,
  `numEpisodes` int(11) NOT NULL,
  `numSeasons` int(11) NOT NULL,
  `retrieveDate` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `series_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKjyaaflhbtrh9vcbwmlk0wj9hn` (`series_id`)
)