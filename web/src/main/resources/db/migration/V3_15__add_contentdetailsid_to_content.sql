-- ContentDetails

ALTER TABLE `films`.`ContentDetails`
    ADD COLUMN `id` INT UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`id`);

ALTER TABLE `films`.`ContentDetails`
    DROP FOREIGN KEY `fk_content_id_content`;

-- SeriesDetails

ALTER TABLE `films`.`SeriesDetails`
    ADD COLUMN `id` INT UNSIGNED NULL FIRST;

UPDATE films.SeriesDetails s set id = (SELECT id FROM films.ContentDetails c where c.content_id = s.content_id);

ALTER TABLE `films`.`SeriesDetails`
    DROP FOREIGN KEY `fk_content_id_contentdetails`;

ALTER TABLE `films`.`SeriesDetails`
    CHANGE COLUMN `id` `id` INT UNSIGNED NOT NULL ,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`id`);

ALTER TABLE `films`.`SeriesDetails`
    CHANGE COLUMN `content_id` `content_id` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ;


-- MovieDetails

ALTER TABLE `films`.`MovieDetails`
    ADD COLUMN `id` INT UNSIGNED NULL FIRST;

UPDATE films.MovieDetails s set id = (SELECT id FROM films.ContentDetails c where c.content_id = s.content_id);

ALTER TABLE `films`.`MovieDetails`
    DROP FOREIGN KEY `fk_movie_content_id_contentdetails`;

ALTER TABLE `films`.`MovieDetails`
    CHANGE COLUMN `id` `id` INT UNSIGNED NOT NULL ,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`id`);

ALTER TABLE `films`.`MovieDetails`
    CHANGE COLUMN `content_id` `content_id` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ;


-- Content

ALTER TABLE `films`.`Content`
    ADD COLUMN `contentDetailsId` INT UNSIGNED NULL AFTER `userId`;

ALTER TABLE `films`.`Content`
    ADD CONSTRAINT `fk_contentdetailsid_contentdetails`
        FOREIGN KEY (`contentDetailsId`)
            REFERENCES `films`.`ContentDetails` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;

update films.Content c set contentDetailsId = (select id from films.ContentDetails cd where cd.content_id = c.id);

-- drop content_id columns

ALTER TABLE `films`.`SeriesDetails`
    DROP COLUMN `content_id`;

ALTER TABLE `films`.`MovieDetails`
    DROP COLUMN `content_id`;

ALTER TABLE `films`.`ContentDetails`
    DROP COLUMN `content_id`,
    DROP INDEX `fk_content_id_content_idx` ;
