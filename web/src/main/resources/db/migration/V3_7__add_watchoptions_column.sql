ALTER TABLE `films`.`ContentDetails`
    ADD COLUMN `watchOptions` VARCHAR(45) NULL DEFAULT NULL;

CREATE TABLE `films`.`content_watch_options`
(
    `content_id`   MEDIUMINT UNSIGNED NOT NULL,
    `watch_option` VARCHAR(15)        NOT NULL,
    PRIMARY KEY (`content_id`, `watch_option`)
);
