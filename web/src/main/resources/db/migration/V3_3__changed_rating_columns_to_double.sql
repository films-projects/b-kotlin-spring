UPDATE films.TmdbMovie
set imdbRating = null
where imdbRating not like '%.%';

UPDATE films.TmdbSeries
set imdbRating = null
where imdbRating not like '%.%';

ALTER TABLE `films`.`TmdbMovie`
    CHANGE COLUMN `rating` `rating` DOUBLE NULL DEFAULT NULL,
    CHANGE COLUMN `imdbRating` `imdbRating` DOUBLE NULL DEFAULT NULL;
ALTER TABLE `films`.`TmdbSeries`
    CHANGE COLUMN `rating` `rating` DOUBLE NULL DEFAULT NULL,
    CHANGE COLUMN `imdbRating` `imdbRating` DOUBLE NULL DEFAULT NULL;
