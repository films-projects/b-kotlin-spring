ALTER TABLE `films`.`TmdbMovie`
    CHANGE COLUMN `retrieveDate` `lastRefresh` DATE NULL DEFAULT NULL ;

ALTER TABLE `films`.`TmdbSeries`
    CHANGE COLUMN `retrieveDate` `lastRefresh` DATE NULL DEFAULT NULL ;
