CREATE TABLE `films`.`Configuration` (
  `configKey` VARCHAR(255) NOT NULL,
  `configValue` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`configKey`),
  UNIQUE INDEX `configKey_UNIQUE` (`configKey` ASC));

INSERT INTO `films`.`Configuration` (`configKey`, `configValue`) VALUES ('tmdb.api.key', '65130860f9e81257cdec0ae1d0c743c7');
