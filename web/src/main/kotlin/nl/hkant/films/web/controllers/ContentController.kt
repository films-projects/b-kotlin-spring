package nl.hkant.films.web.controllers

import nl.hkant.films.common.model.User
import nl.hkant.films.common.services.ContentService
import nl.hkant.films.web.services.RecentAndUpcomingContentService
import nl.hkant.films.web.services.SeriesAndEpisodes
import nl.hkant.films.web.services.UpcomingContentDTO
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import java.time.LocalDate

@Controller
@RequestMapping("/content")
class ContentController(
    private val contentService: ContentService,
    private val recentAndUpcomingContentService: RecentAndUpcomingContentService,
) {

    @PostMapping("/{id}/update")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun update(
        @AuthenticationPrincipal user: User,
        @PathVariable id: Int,
        @RequestParam title: String?,
        @RequestParam year: Int?,
        @RequestParam rating: Int?,
        @RequestParam specialInterest: Boolean?,
        @RequestParam watching: Boolean?,
    ) {
        contentService.update(
            user = user,
            id = id,
            title = title,
            year = year,
            rating = rating,
            specialInterest = specialInterest,
            watching = watching,
        )
    }

    @PostMapping("/{id}/delete")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@AuthenticationPrincipal user: User, @PathVariable id: Int) {
        contentService.delete(user, id)
    }

    @GetMapping("/upcoming")
    @ResponseBody
    fun upcoming(
        @AuthenticationPrincipal user: User,
        @RequestParam(required = false, defaultValue = "90") days: Long,
    ): UpcomingContent {
        val seriesAndEpisodes = recentAndUpcomingContentService.getLastAndNextEpisodesForWatchingSeries(user)
        val grouped = seriesAndEpisodes.groupBy { it.nextEpisode == null }
        val series = (grouped[false] ?: listOf()).sortedBy { it.nextEpisode?.airDate?.toString() ?: "z" } +
            (grouped[true] ?: listOf()).sortedBy { it.lastEpisode?.airDate?.toString() ?: "z" }.reversed()
        val recentMovies = recentAndUpcomingContentService.getRecentMovies(user, LocalDate.now().minusDays(days))
        val upcomingMovies = recentAndUpcomingContentService.getUpcomingMovies(user, LocalDate.now().plusDays(days))
        return UpcomingContent(
            series = series,
            recentMovies = recentMovies.sortedBy { it.releaseDate },
            upcomingMovies = upcomingMovies.sortedBy { it.releaseDate },
        )
    }
}

data class UpcomingContent(
    val series: List<SeriesAndEpisodes>,
    val upcomingMovies: List<UpcomingContentDTO>,
    val recentMovies: List<UpcomingContentDTO>,
)
