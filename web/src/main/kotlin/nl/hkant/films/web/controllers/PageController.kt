package nl.hkant.films.web.controllers

import nl.hkant.films.web.services.BackgroundImageService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class PageController(
    private val backgroundImageService: BackgroundImageService,
) {

    @GetMapping("/search", "/")
    fun search(): ModelAndView =
        ModelAndView("search", mapOf("background" to backgroundImageService.randomContentOrDefaultBackgroundImage()))

    @GetMapping("/login")
    fun login(): ModelAndView =
        ModelAndView("login", mapOf("background" to backgroundImageService.randomDefaultBackgroundImage()))

    @GetMapping("/upcoming")
    fun upcoming(): ModelAndView =
        ModelAndView("upcoming", mapOf("background" to backgroundImageService.randomContentOrDefaultBackgroundImage()))
}
