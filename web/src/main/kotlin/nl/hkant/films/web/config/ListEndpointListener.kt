package nl.hkant.films.web.config

import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Profile
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.stereotype.Component
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping

private val logger = LoggerFactory.getLogger(ListEndpointListener::class.java)

@Component
@Profile("listendpoints")
class ListEndpointListener : ApplicationListener<ContextRefreshedEvent> {
    override fun onApplicationEvent(event: ContextRefreshedEvent) {
        val applicationContext = event.applicationContext
        val requestMappingHandlerMapping = applicationContext
            .getBean("requestMappingHandlerMapping", RequestMappingHandlerMapping::class.java)
        val map = requestMappingHandlerMapping.getHandlerMethods()
        map
            .entries
            .toList()
            .sortedBy { it.key.patternValues.firstOrNull() }
            .forEach { (key, value) ->
                logger.info("{} {}", key, value)
            }
    }
}
