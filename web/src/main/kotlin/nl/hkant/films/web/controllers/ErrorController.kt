package nl.hkant.films.web.controllers

import jakarta.servlet.RequestDispatcher
import jakarta.servlet.http.HttpServletRequest
import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class ErrorController : ErrorController {

    @GetMapping("/error")
    fun error(request: HttpServletRequest): ModelAndView {
        val status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE) as? Int
        val view = when (status) {
            HttpStatus.NOT_FOUND.value() -> "404"
            else -> "error"
        }
        return ModelAndView(view)
    }
}
