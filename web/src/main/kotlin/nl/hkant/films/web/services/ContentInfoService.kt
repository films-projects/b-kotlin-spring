package nl.hkant.films.web.services

import nl.hkant.films.common.dao.ContentDao
import nl.hkant.films.common.dao.ContentDetailsDao
import nl.hkant.films.common.model.NotFoundException
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.Content
import nl.hkant.films.common.model.content.ContentDetails
import nl.hkant.films.common.model.content.MovieDetails
import nl.hkant.films.common.model.content.SeriesDetails
import nl.hkant.films.common.model.content.WatchOption.Companion.toWatchOption
import nl.hkant.films.common.services.ExternalInfoCachingService
import nl.hkant.films.common.util.convertToContentDetails
import nl.hkant.films.external.ContentType
import nl.hkant.films.external.ExtendedContentInfo
import nl.hkant.films.external.IExternalContentInfoFetchService
import nl.hkant.films.external.IImdbRatingService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDate

@Service
class ContentInfoService(
    private val contentDao: ContentDao,
    private val externalContentInfoFetchService: IExternalContentInfoFetchService,
    private val contentDetailsDao: ContentDetailsDao,
    private val externalInfoCachingService: ExternalInfoCachingService,
    private val imdbRatingService: IImdbRatingService,
) {

    @Transactional
    fun connect(user: User, contentId: Int, externalContentId: String): ContentDetails? {
        val content = contentDao.findByIdAndUserIdOrException(user, contentId)
        val contentDetails = if (externalContentId.startsWith("tt")) {
            val imdbResult = externalContentInfoFetchService.findByImdbId(externalContentId, content.contentType)
            imdbResult?.let { contentDetailsDao.getExistingContentDetails(it.externalContentId, it.contentType) }
                ?: imdbResult?.convertToContentDetails()
        } else {
            contentDetailsDao.getExistingContentDetails(externalContentId.toInt(), content.contentType)
                ?: externalContentInfoFetchService.findByExternalContentId(externalContentId.toInt(), content.contentType)
                    ?.convertToContentDetails()
        }
        return contentDetails
            ?.apply {
                watchOptions = retrieveWatchProviders(this.externalContentId, content.contentType).toMutableSet()
                imdbRating = imdbId?.let { imdbId -> imdbRatingService.getImdbRating(imdbId) }
            }
            ?.also {
                content.contentDetails = it
            }
    }

    @Transactional
    fun disconnect(user: User, contentId: Int): Content =
        contentDao.findByIdAndUserIdOrException(user, contentId)
            .also {
                it.contentDetails?.also { cd ->
                    cd.content.remove(it)
                    if (cd.content.isEmpty()) {
                        contentDetailsDao.delete(cd)
                    }
                }
                it.contentDetails = null
                contentDao.save(it)
            }

    fun searchByName(user: User, title: String, type: ContentType): List<ContentDetails> = when (type) {
        ContentType.FILM -> externalContentInfoFetchService.searchMovie(title).map {
            it.convertToContentDetails().apply {
                contentTitle = it.title
            }
        }
        ContentType.SERIES -> externalContentInfoFetchService.searchSeries(title).map {
            it.convertToContentDetails().apply {
                contentTitle = it.title
            }
        }
    }

    @Transactional
    fun refresh(user: User, contentId: Int): ContentDetails {
        val contentDetails = contentDao.findByIdAndUserId(user, contentId)?.contentDetails
            ?: throw NotFoundException()
        return refreshInner(contentDetails)
    }

    @Transactional
    fun refreshDangerous(contentDetails: ContentDetails): ContentDetails {
        return refreshInner(contentDetails)
    }

    private fun refreshInner(info: ContentDetails): ContentDetails {
        when (info) {
            is MovieDetails ->
                externalContentInfoFetchService
                    .findByExternalContentId(info.externalContentId, ContentType.FILM)
                    ?.convertToContentDetails()
                    ?.let { it as MovieDetails }
                    ?.apply {
                        imdbRating = imdbId?.let { imdbId -> imdbRatingService.getImdbRating(imdbId) }
                        watchOptions = retrieveWatchProviders(externalContentId, ContentType.FILM).toMutableSet()
                    }
                    ?.also {
                        info.update(it)
                    }

            is SeriesDetails -> externalContentInfoFetchService.findByExternalContentId(
                info.externalContentId,
                ContentType.SERIES,
            )
                ?.convertToContentDetails()
                ?.let { it as SeriesDetails }
                ?.apply {
                    imdbRating = imdbId?.let { imdbId -> imdbRatingService.getImdbRating(imdbId) }
                    watchOptions = retrieveWatchProviders(externalContentId, ContentType.FILM).toMutableSet()
                }
                ?.also {
                    info.update(it)
                }
        }
        info.lastRefresh = LocalDate.now()
        return contentDetailsDao.save(info)
    }

    private fun retrieveWatchProviders(externalContentId: Int, contentType: ContentType) =
        externalInfoCachingService.getStreamingProviders(externalContentId, contentType)
            .map { sp -> sp.toWatchOption() }

    fun getFullContentInfo(user: User, contentId: Int): FullContentInfo {
        return contentDao.findByIdAndUserIdOrException(user, contentId)
            .let {
                FullContentInfo(
                    it,
                    it.getContentInfo(),
                    it.getContentInfo()?.let { c ->
                        externalContentInfoFetchService.getAdditionalContentInfo(c.externalContentId, it.contentType)
                    },
                )
            }
    }
}

data class FullContentInfo(
    val content: Content,
    val contentDetails: ContentDetails? = null,
    val extendedContentInfo: ExtendedContentInfo? = null,
)
