package nl.hkant.films.web.controllers

import nl.hkant.films.common.dao.ContentDao
import nl.hkant.films.common.dao.SeriesDao
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.Series
import nl.hkant.films.common.services.ContentService
import nl.hkant.films.external.ContentType
import nl.hkant.films.web.dto.NewContentDto
import nl.hkant.films.web.dto.NewContentFromExternalDto
import nl.hkant.films.web.services.RecentAndUpcomingContentService
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/series")
class SeriesController(
    private val contentService: ContentService,
    private val contentDao: ContentDao,
    private val seriesDao: SeriesDao,
    private val recentAndUpcomingContentService: RecentAndUpcomingContentService,
) {

    @PostMapping("/new")
    fun new(
        @AuthenticationPrincipal user: User,
        @RequestBody series: Series,
    ): NewContentDto =
        series
            .apply {
                seen = myRating != 0
                userId = user.id
            }
            .let { contentDao.save(it) }
            .let { NewContentDto(it.title, it.id, ContentType.SERIES) }

    @PostMapping("/newFromExternal/{externalContentId}")
    fun newFromExternal(
        @AuthenticationPrincipal user: User,
        @PathVariable externalContentId: Int,
        @RequestParam(required = false, defaultValue = "false") interested: Boolean,
    ): NewContentFromExternalDto =
        contentService.createFromExternalResource(user, externalContentId, ContentType.SERIES, interested)
            ?.let { NewContentFromExternalDto(it.title) }
            ?: throw RuntimeException("Could not find external content")

    @GetMapping("/{id}/episodeInfo")
    fun episodeInfo(@PathVariable id: Int): nl.hkant.films.external.LastAndNextEpisode =
        seriesDao.findById(id)?.contentDetails?.externalContentId
            ?.let { recentAndUpcomingContentService.getLastAndNextEpisodes(it) }
            ?: nl.hkant.films.external.LastAndNextEpisode()

    @GetMapping("/upcoming")
    fun upcomingEpisodes(@AuthenticationPrincipal user: User): List<UpcomingSeries> =
        recentAndUpcomingContentService.getLastAndNextEpisodesForWatchingSeries(user)
            .filter { it.nextEpisode != null }
            .sortedBy { it.nextEpisode?.airDate?.toString() ?: "z" }
            .map { UpcomingSeries(it.series.title, it.nextEpisode!!) }

    data class UpcomingSeries(val title: String, val episode: nl.hkant.films.external.EpisodeInfo)
}
