package nl.hkant.films.web.dto

import nl.hkant.films.common.model.content.SeriesStatus
import nl.hkant.films.common.model.content.WatchOption

data class SearchParams(
    val title: String? = null,
    val yearFrom: Int? = null,
    val yearTo: Int? = null,
    val anticipated: Boolean? = null,
    val myRating: List<Int>? = null,
    val watch: List<WatchOption>? = null,
    val type: String? = null,
    val minRating: Double? = null,
    // film
    val released: Boolean? = null,
    // series
    val watching: Boolean? = null,
    val status: SeriesStatus? = null,
)
