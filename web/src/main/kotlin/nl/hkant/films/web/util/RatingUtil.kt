package nl.hkant.films.web.util

import java.math.BigDecimal
import java.math.RoundingMode

fun Double.roundToOneDecimal() =
    roundToDecimals(1)

fun Double.roundToDecimals(scale: Int = 1) =
    BigDecimal.valueOf(this).setScale(scale, RoundingMode.HALF_EVEN).toDouble()
