package nl.hkant.films.web.controllers

import nl.hkant.films.common.dao.SearchFavoriteDao
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.search.SearchFavorite
import nl.hkant.films.common.util.ImageUtil
import nl.hkant.films.web.dto.SearchParams
import nl.hkant.films.web.dto.Slide
import nl.hkant.films.web.services.SearchService
import org.springframework.data.domain.Pageable
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/search")
@RestController
class SearchController(
    private val searchService: SearchService,
    private val searchFavoriteDao: SearchFavoriteDao,
    private val imageUtil: ImageUtil,
) {
    @GetMapping("/do")
    fun doSearch(
        @AuthenticationPrincipal user: User,
        searchParams: SearchParams,
        pageable: Pageable,
    ): SearchResult {
        val timestamp = System.currentTimeMillis()
        return searchService.findByParams(user, searchParams, pageable).let {
            SearchResult(
                results = SearchResults(
                    content = it.content.map { c ->
                        Slide(c, posterImage = c.contentDetails?.posterPath?.let { p -> imageUtil.toPosterImageUrl(p) })
                    },
                    number = it.number,
                    totalPages = it.totalPages,
                    totalElements = it.totalElements,
                ),
                timestamp = timestamp,
            )
        }
    }

    @GetMapping("/favorites")
    fun getFavorites(@AuthenticationPrincipal user: User) =
        searchFavoriteDao.findByUserIdReadOnly(user.id).map { SearchQuery(it.query, it.params()) }

    @PostMapping("/favorites/add")
    fun addFavorite(@AuthenticationPrincipal user: User, @RequestBody query: SearchQuery) =
        SearchFavorite(userId = user.id, query = query.query)
            .also { searchFavoriteDao.save(it) }
            .let { SearchQuery(it.query, it.params()) }

    @PostMapping("/favorites/remove")
    fun removeFavorite(@AuthenticationPrincipal user: User, @RequestBody query: SearchQuery): List<SearchQuery> {
        searchFavoriteDao.removeByQuery(user.id, query.query)
        return getFavorites(user)
    }
}

data class SearchQuery(
    val query: String,
    val params: Map<String, List<String>> = emptyMap(),
)

data class SearchResults(
    val content: List<Slide>,
    val number: Number,
    val totalPages: Number,
    val totalElements: Number,
)

data class SearchResult(
    val results: SearchResults,
    val timestamp: Long,
)
