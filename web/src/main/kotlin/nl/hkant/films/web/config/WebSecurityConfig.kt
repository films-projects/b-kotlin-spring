package nl.hkant.films.web.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.invoke
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository
import org.springframework.security.web.savedrequest.HttpSessionRequestCache
import javax.sql.DataSource

@Configuration
@EnableWebSecurity
class WebSecurityConfig(
    private val dataSource: DataSource,
    private val myUserDetailsService: UserDetailsService,
) {

    @Bean
    fun filterChain(http: HttpSecurity): SecurityFilterChain {
        http.invoke {
            csrf {
                disable()
            }
            formLogin {
                permitAll()
                loginPage = "/login"
                loginProcessingUrl = "/login"
                defaultSuccessUrl("/search", true)
            }
            authorizeHttpRequests {
                setOf(
                    "/login",
                    "/error",
                    "/actuator/**",
                    "/css/**",
                    "/js/**",
                    "/favicon.ico",
                    "/img/background/**",
                ).forEach {
                    authorize(it, permitAll)
                }

                authorize(anyRequest, authenticated)
            }
            logout {
                logoutSuccessUrl = "/login?logout"
            }
            exceptionHandling {
                accessDeniedPage = "/login"
            }
            requestCache {
                requestCache = HttpSessionRequestCache().apply { setMatchingRequestParameterName(null) }
            }
            rememberMe {
                rememberMeParameter = "rememberme"
                tokenRepository = persistentTokenRepository()
                tokenValiditySeconds = 60 * 60 * 24 * 30
                userDetailsService = myUserDetailsService
            }
        }
        return http.build()
    }

    @Bean
    fun persistentTokenRepository(): PersistentTokenRepository? {
        return JdbcTokenRepositoryImpl().also { it.setDataSource(dataSource) }
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder(11)
    }

    @Bean
    fun authenticationProvider(): AuthenticationProvider {
        return DaoAuthenticationProvider().apply {
            setUserDetailsService(myUserDetailsService)
            setPasswordEncoder(passwordEncoder())
        }
    }
}
