package nl.hkant.films.web.config

import nl.hkant.films.common.config.CommonConfig
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(CommonConfig::class)
@ComponentScan("nl.hkant.films.web")
class WebConfig
