package nl.hkant.films.web.controllers

import nl.hkant.films.common.dao.ConfigDao
import nl.hkant.films.common.dao.ContentDao
import nl.hkant.films.common.model.ConfigKey
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.MovieDetails
import nl.hkant.films.common.model.content.WatchOption
import nl.hkant.films.common.model.content.WatchOption.Companion.toWatchOption
import nl.hkant.films.external.ContentType
import nl.hkant.films.external.IStreamingProviderLinkService
import nl.hkant.films.external.IStreamingProviderService
import nl.hkant.films.external.StreamingLinkResult
import nl.hkant.films.web.services.BackgroundImageService
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.ModelAndView

@Controller
@RequestMapping("/watch")
class WatchController(
    private val streamingProviderService: IStreamingProviderService,
    private val backgroundImageService: BackgroundImageService,
    private val streamingProviderLinkService: IStreamingProviderLinkService,
    private val contentDao: ContentDao,
    private val configDao: ConfigDao,
) {

    @GetMapping("/options")
    @ResponseBody
    fun watchOptions(): List<WatchOption> =
        runCatching {
            configDao.getOrDefault(ConfigKey.USABLE_WATCH_OPTIONS, "")
                .split(",")
                .map { WatchOption.valueOf(it) }
        }.getOrDefault(emptyList())

    @GetMapping("/{id}/{provider}")
    fun getWatchLink(
        @AuthenticationPrincipal user: User,
        @PathVariable id: Int,
        @PathVariable provider: WatchOption,
    ): ModelAndView {
        val content = contentDao.findByIdAndUserIdOrException(user, id)
        val contentDetails = content.contentDetails
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "resource not found")

        val result = if (user.isAdmin()) {
            streamingProviderLinkService.getStreamingLink(contentDetails.imdbId, provider.toStreamingProvider())
        } else {
            StreamingLinkResult.NotFound()
        }

        val contentType = if (contentDetails is MovieDetails) {
            ContentType.FILM
        } else {
            ContentType.SERIES
        }

        val externalListOfProviders = streamingProviderService.getLinkToExternalListOfStreamingProviders(
            contentDetails.externalContentId,
            contentType,
        )

        return when (result) {
            is StreamingLinkResult.Link ->
                ModelAndView("redirect:${result.link}")

            is StreamingLinkResult.NotFound ->
                if (result.alternatives.isEmpty()) {
                    ModelAndView(
                        "redirect:$externalListOfProviders",
                    )
                } else {
                    ModelAndView(
                        "alternativeStreamingLinks",
                        mapOf(
                            "background" to backgroundImageService.randomDefaultBackgroundImage(),
                            "title" to content.title,
                            "provider" to provider,
                            "alternatives" to result.alternatives.map { AlternativeWatchOption(it) } +
                                AlternativeWatchOption(
                                    name = "TMDB",
                                    link = externalListOfProviders,
                                    icon = "/img/external/tmdb.svg",
                                ),
                        ),
                    )
                }

            else ->
                throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "resource not found")
        }
    }
}

data class AlternativeWatchOption(val name: String, val link: String, val icon: String?) {
    constructor(alternative: StreamingLinkResult.NotFound.Alternative) : this(
        alternative.streamingProvider.toWatchOption().friendlyName,
        alternative.link,
        alternative.streamingProvider.toWatchOption().getIcon(),
    )
}

fun WatchOption.getIcon() =
    "/img/external/" + when (this) {
        WatchOption.NETFLIX -> "netflix.svg"
        WatchOption.AMAZON_PRIME -> "prime.svg"
        WatchOption.SKY_SHOWTIME -> "showtime.svg"
        WatchOption.HBO_MAX -> "hbomax.svg"
        WatchOption.APPLE_TV -> "apple.svg"
    }
