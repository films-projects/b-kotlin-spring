package nl.hkant.films.web.services

import nl.hkant.films.common.dao.ConfigDao
import nl.hkant.films.common.dao.ContentDetailsDao
import nl.hkant.films.common.model.ConfigKey
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.concurrent.TimeUnit

private val logger: Logger = LoggerFactory.getLogger(RefreshContentDetailsService::class.java)

@Service
@Profile("refreshContent")
class RefreshContentDetailsService(
    private val contentDetailsDao: ContentDetailsDao,
    private val contentInfoService: ContentInfoService,
    configDao: ConfigDao,
) {

    private val daysLimit: Int = configDao.getOrDefault(ConfigKey.SCHEDULED_REFRESH_DAY_LIMIT, "0").toInt()

    @Scheduled(fixedDelay = 5 * 60 * 1_000L)
    fun refreshContent() {
        val limit = LocalDate.now().minusDays(daysLimit.toLong())
        contentDetailsDao.getOneLastRefreshBefore(limit)
            ?.also {
                logger.info(
                    "Refreshing content details {}, last refresh was {}, date limit was {}",
                    it.id,
                    it.lastRefresh,
                    limit,
                )
                contentInfoService.refreshDangerous(it)
            }
            ?: run {
                logger.info(
                    "Found no content to refresh with date before {}, sleeping {} day(s)",
                    limit,
                    daysLimit + 1,
                )
                Thread.sleep(TimeUnit.DAYS.toMillis((daysLimit + 1L)))
            }
    }
}
