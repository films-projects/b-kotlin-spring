package nl.hkant.films.web.dto

import nl.hkant.films.common.model.content.Content
import nl.hkant.films.common.model.content.ContentDetails
import nl.hkant.films.common.model.content.MovieDetails
import nl.hkant.films.common.model.content.Series
import nl.hkant.films.common.model.content.SeriesDetails
import nl.hkant.films.common.model.content.WatchOption
import java.util.Locale

class Slide(
    content: Content,
    contentDetails: ContentDetails? = content.contentDetails,
    // top
    val id: Int = content.id,
    val type: String = content.javaClass.simpleName.lowercase(Locale.getDefault()),
    val title: String? = content.title,
    val year: Int = content.year,
    val seen: Boolean = content.seen,
    val anticipated: Boolean = content.anticipated,
    val myRating: Int = content.myRating,
    val watching: Boolean? = (content as? Series)?.watching,

    val streamingPlatforms: Set<WatchOption> = contentDetails?.watchOptions ?: emptySet(),
    val connected: Boolean = contentDetails != null,
    val overview: String? = contentDetails?.overview,
    val imdbRating: Double? = contentDetails?.imdbRating,
    val imdbId: String? = contentDetails?.imdbId,
    val runtime: Int = (contentDetails as? MovieDetails)?.runtime ?: 0,
    val status: String? = (contentDetails as? SeriesDetails)?.status?.friendlyName,
    val episodeRuntime: Int = (contentDetails as? SeriesDetails)?.episodeRuntime ?: 0,
    val numEpisodes: Int = (contentDetails as? SeriesDetails)?.numEpisodes ?: 0,
    val posterImage: String?,
)
