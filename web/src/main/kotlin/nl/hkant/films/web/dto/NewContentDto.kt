package nl.hkant.films.web.dto

import nl.hkant.films.external.ContentType

data class NewContentDto(
    val title: String,
    val id: Int,
    val type: ContentType,
)

data class NewContentFromExternalDto(
    val title: String,
)
