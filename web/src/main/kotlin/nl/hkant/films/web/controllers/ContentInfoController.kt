package nl.hkant.films.web.controllers

import nl.hkant.films.common.dao.ContentDao
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.ContentDetails
import nl.hkant.films.common.model.content.MovieDetails
import nl.hkant.films.common.model.content.SeriesDetails
import nl.hkant.films.common.model.content.WatchOption
import nl.hkant.films.common.util.ImageUtil
import nl.hkant.films.external.ContentType
import nl.hkant.films.web.dto.Slide
import nl.hkant.films.web.services.ContentInfoService
import nl.hkant.films.web.services.FullContentInfo
import nl.hkant.films.web.util.roundToOneDecimal
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.util.Locale

@RestController
@RequestMapping("/contentInfo")
class ContentInfoController(
    private val contentInfoService: ContentInfoService,
    private val contentDao: ContentDao,
    private val imageUtil: ImageUtil,
) {

    @GetMapping("/search/{type}/{title}")
    fun searchName(
        @AuthenticationPrincipal user: User,
        @PathVariable title: String,
        @PathVariable type: ContentType,
    ) = contentInfoService.searchByName(user, title, type).map {
        ContentInfoSearchResult(
            contentDetails = it,
            contentHasBeenAdded =
            contentDao.findByExternalContentIdForUser(user, it.externalContentId, type) != null,
            posterImage = it.posterPath?.let { p -> imageUtil.toPosterImageUrl(p) },
        )
    }

    @PostMapping("/{id}/connect/{externalId}")
    fun connect(
        @AuthenticationPrincipal user: User,
        @PathVariable id: Int,
        @PathVariable externalId: String,
    ): ContentInfo =
        contentInfoService.connect(user, id, externalId)
            ?.let { ContentInfo(it, posterImage = it.posterPath?.let { p -> imageUtil.toPosterImageUrl(p) }) }
            ?: throw NullPointerException("The content info was connected but somehow not set")

    @PostMapping("/{id}/disconnect")
    fun disconnect(@AuthenticationPrincipal user: User, @PathVariable id: Int) =
        contentInfoService.disconnect(user, id).let { content ->
            Slide(content, posterImage = content.contentDetails?.posterPath?.let { imageUtil.toPosterImageUrl(it) })
        }

    @PostMapping("/{id}/refresh")
    fun refresh(@AuthenticationPrincipal user: User, @PathVariable id: Int): ContentInfo =
        contentInfoService.refresh(user, id).let {
            ContentInfo(it, posterImage = it.posterPath?.let { p -> imageUtil.toPosterImageUrl(p) })
        }

    @GetMapping("/{id}/full")
    fun full(
        @AuthenticationPrincipal user: User,
        @PathVariable id: Int,
    ): FullContentInfoDTO =
        contentInfoService.getFullContentInfo(user, id).let {
            FullContentInfoDTO(
                it,
                posterImage = it.contentDetails?.posterPath?.let { p -> imageUtil.toPosterImageUrl(p) },
            )
        }
}

class ContentInfoSearchResult(
    contentDetails: ContentDetails,
    val contentHasBeenAdded: Boolean = false,
    val title: String? = contentDetails.contentTitle,
    val externalId: Int = contentDetails.externalContentId,
    val overview: String? = contentDetails.overview,
    val releaseDate: String? = contentDetails.getReleaseDate().toString(),
    val posterImage: String?,
)

class ContentInfo(
    contentDetails: ContentDetails,
    val type: String = if (contentDetails is MovieDetails) "film" else "series",
    val overview: String? = contentDetails.overview,
    val imdbRating: Double? = contentDetails.imdbRating?.roundToOneDecimal(),
    val imdbId: String? = contentDetails.imdbId,
    val runtime: Int = (contentDetails as? MovieDetails)?.runtime ?: 0,
    val status: String? = (contentDetails as? SeriesDetails)?.status?.friendlyName,
    val episodeRuntime: Int = (contentDetails as? SeriesDetails)?.episodeRuntime ?: 0,
    val numEpisodes: Int = (contentDetails as? SeriesDetails)?.numEpisodes ?: 0,
    val streamingPlatforms: Set<WatchOption> = contentDetails.watchOptions,
    val posterImage: String?,
)

class FullContentInfoDTO(
    info: FullContentInfo,
    val id: Int = info.content.id,
    val title: String = info.content.title,
    val type: String = info.content::class.simpleName?.lowercase(Locale.getDefault()) ?: "",
    val year: Int = info.content.year,
    val seen: Boolean = info.content.seen,
    val myRating: Int = info.content.myRating,
    val streamingPlatforms: Set<WatchOption> = info.contentDetails?.watchOptions ?: emptySet(),
    val overview: String? = info.contentDetails?.overview,
    val imdbId: String? = info.contentDetails?.imdbId,
    val imdbRating: Double? = info.contentDetails?.imdbRating,
    val runtime: Int = (info.contentDetails as? MovieDetails)?.runtime ?: 0,
    val status: String? = (info.contentDetails as? SeriesDetails)?.status?.friendlyName,
    val episodeRuntime: Int = (info.contentDetails as? SeriesDetails)?.episodeRuntime ?: 0,
    val numEpisodes: Int = (info.contentDetails as? SeriesDetails)?.numEpisodes ?: 0,
    val cast: List<String> = info.extendedContentInfo?.cast ?: emptyList(),
    val genres: List<String> = info.extendedContentInfo?.genres ?: emptyList(),
    val originalLanguage: String? = info.extendedContentInfo?.originalLanguage,
    val lastRefresh: LocalDate? = info.contentDetails?.lastRefresh,
    val posterImage: String?,
)
