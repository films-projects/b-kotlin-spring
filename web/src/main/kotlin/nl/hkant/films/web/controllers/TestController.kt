package nl.hkant.films.web.controllers

import nl.hkant.films.common.model.NotFoundException
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/test")
@Profile("dev")
class TestController {

    @GetMapping("/error/404", "/")
    fun error404() {
        throw NotFoundException()
    }

    @GetMapping("/error/500")
    fun error500() {
        throw RuntimeException()
    }
}
