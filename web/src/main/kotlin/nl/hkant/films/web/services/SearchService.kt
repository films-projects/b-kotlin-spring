package nl.hkant.films.web.services

import nl.hkant.films.common.dao.ContentDao
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.Content
import nl.hkant.films.web.dto.SearchParams
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class SearchService(
    private val contentDao: ContentDao,
) {
    fun findByParams(user: User, searchParams: SearchParams, pageable: Pageable): Page<Content> =
        contentDao.findByParams(
            user = user,
            pageable = pageable,
            title = searchParams.title,
            yearFrom = searchParams.yearFrom,
            yearTo = searchParams.yearTo,
            anticipated = searchParams.anticipated,
            myRating = searchParams.myRating,
            type = searchParams.type,
            minRating = searchParams.minRating,
            watchOptions = searchParams.watch,
            watching = searchParams.watching.takeUnless { searchParams.type == "film" },
            seriesStatus = searchParams.status.takeUnless { searchParams.type == "film" },
            released = searchParams.released.takeIf { searchParams.type == "film" },
        )
}
