package nl.hkant.films.web.services

import nl.hkant.films.common.dao.UserDao
import nl.hkant.films.common.model.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CustomUserDetailsService(
    private val userDao: UserDao,
) : UserDetailsService {

    override fun loadUserByUsername(username: String?): User? {
        return username?.let { userDao.findByUsername(it) } ?: throw UsernameNotFoundException(username)
    }
}
