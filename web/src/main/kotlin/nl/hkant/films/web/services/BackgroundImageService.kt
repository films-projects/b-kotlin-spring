package nl.hkant.films.web.services

import nl.hkant.films.common.dao.ContentDetailsDao
import nl.hkant.films.common.util.ImageUtil
import org.springframework.stereotype.Service

@Service
class BackgroundImageService(
    private val contentDetailsDao: ContentDetailsDao,
    private val imageUtil: ImageUtil,
) {
    private val filenames: List<String> = listOf(
        "1.jpeg",
        "2.jpg",
        "3.png",
        "4.jpg",
        "5.png",
        "6.jpg",
        "7.png",
        "8.jpg",
        "9.jpg",
        "10.jpg",
        "11.jpg",
        "12.jpg",
        "13.jpg",
        "14.jpg",
        "15.jpg",
        "16.jpg",
        "17.jpg",
        "18.jpg",
    )
    private val path = "/img/background/"

    fun randomContentOrDefaultBackgroundImage(): String {
        return contentDetailsDao.getRandomBackdrop()
            ?.let { imageUtil.toBackgroundImageUrl(it) }
            ?: randomDefaultBackgroundImage()
    }

    fun randomDefaultBackgroundImage(): String {
        return "$path${filenames.random()}"
    }
}
