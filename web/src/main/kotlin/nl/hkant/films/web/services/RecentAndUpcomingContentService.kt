package nl.hkant.films.web.services

import nl.hkant.films.common.dao.FilmDao
import nl.hkant.films.common.dao.SeriesDao
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.Content
import nl.hkant.films.common.model.content.SeriesDetails
import nl.hkant.films.common.model.content.SeriesStatus
import nl.hkant.films.common.services.ExternalInfoCachingService
import nl.hkant.films.common.util.toPosterImageUrl
import nl.hkant.films.external.LastAndNextEpisode
import org.springframework.stereotype.Service
import java.io.Serializable
import java.time.LocalDate

/**
 * Service providing info about the content that is (soon) to be released
 */
@Service
class RecentAndUpcomingContentService(
    private val seriesDao: SeriesDao,
    private val filmDao: FilmDao,
    private val externalInfoCachingService: ExternalInfoCachingService,
) {

    fun getLastAndNextEpisodesForWatchingSeries(user: User): List<SeriesAndEpisodes> =
        seriesDao.findByWatchingOrAnticipatedIsTrue(user)
            .filter { it.getContentInfo()?.status in setOf(SeriesStatus.ONGOING, SeriesStatus.UPCOMING) }
            .map { Pair(it, getLastAndNextEpisodes(it.contentDetails!!.externalContentId)) }
            .map { (content, episodes) ->
                SeriesAndEpisodes(
                    series = UpcomingContentDTO(content),
                    lastEpisode = episodes.lastEpisode,
                    nextEpisode = episodes.nextEpisode,
                )
            }

    fun getLastAndNextEpisodes(externalContentId: Int): LastAndNextEpisode =
        externalInfoCachingService.getLastAndUpcomingEpisodes(externalContentId)

    fun getRecentMovies(user: User, since: LocalDate): List<UpcomingContentDTO> =
        filmDao.getUnseenByAirDateBetween(
            user = user,
            startDateExcl = since,
            endDateIncl = LocalDate.now(),
            filterSeen = true,
        )
            .map { UpcomingContentDTO(it) }

    fun getUpcomingMovies(user: User, untilExclusive: LocalDate): List<UpcomingContentDTO> =
        filmDao.getUnseenByAirDateBetween(user, LocalDate.now(), untilExclusive)
            .map { UpcomingContentDTO(it) }
}

data class SeriesAndEpisodes(
    val series: UpcomingContentDTO,
    val lastEpisode: nl.hkant.films.external.EpisodeInfo?,
    val nextEpisode: nl.hkant.films.external.EpisodeInfo?,
) : Serializable

class UpcomingContentDTO(
    content: Content,
    val id: Int = content.id,
    val title: String = content.title,
    val specialInterest: Boolean = content.anticipated,
    val posterUrl: String? = content.contentDetails?.posterPath?.toPosterImageUrl(),
    val releaseDate: String? = content.contentDetails?.getReleaseDate()?.toString(),
    val status: String? = content.contentDetails?.let { it as? SeriesDetails }?.status?.friendlyName,
) : Serializable
