package nl.hkant.films.web.controllers

import nl.hkant.films.common.dao.ContentDao
import nl.hkant.films.common.dao.FilmDao
import nl.hkant.films.common.model.User
import nl.hkant.films.common.model.content.Film
import nl.hkant.films.common.model.content.WatchOption
import nl.hkant.films.common.services.ContentService
import nl.hkant.films.common.util.ImageUtil
import nl.hkant.films.external.ContentType
import nl.hkant.films.web.dto.NewContentDto
import nl.hkant.films.web.dto.NewContentFromExternalDto
import nl.hkant.films.web.services.ContentInfoService
import nl.hkant.films.web.services.FullContentInfo
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/film")
class FilmController(
    private val contentService: ContentService,
    private val contentInfoService: ContentInfoService,
    private val filmDao: FilmDao,
    private val contentDao: ContentDao,
    private val imageUtil: ImageUtil,
) {

    @PostMapping("/new")
    fun new(
        @AuthenticationPrincipal user: User,
        @RequestBody film: Film,
    ): NewContentDto =
        film
            .apply {
                seen = myRating != 0
                userId = user.id
            }
            .let { contentDao.save(it) }
            .let { NewContentDto(it.title, it.id, ContentType.FILM) }

    @PostMapping("/newFromExternal/{externalContentId}")
    fun newFromExternal(
        @AuthenticationPrincipal user: User,
        @PathVariable externalContentId: Int,
        @RequestParam(required = false, defaultValue = "false") interested: Boolean,
    ): NewContentFromExternalDto =
        contentService.createFromExternalResource(user, externalContentId, ContentType.FILM, interested)
            ?.title
            ?.let { NewContentFromExternalDto(it) }
            ?: throw RuntimeException("Could not find external content")

    @GetMapping("/suggest")
    fun suggest(
        @AuthenticationPrincipal user: User,
        @RequestParam seen: Boolean,
        @RequestParam anticipated: Boolean?,
        @RequestParam minRating: Double?,
        @RequestParam watchOptions: Set<WatchOption>?,
    ): FullContentInfoDTO {
        val fullInfo = filmDao.findSuggestion(
            user = user,
            minRating = minRating,
            seen = seen,
            anticipated = anticipated,
            watchOptions = watchOptions,
        )
            ?.id
            ?.let { contentInfoService.getFullContentInfo(user, it) }
            ?: FullContentInfo(Film())

        return FullContentInfoDTO(
            fullInfo,
            posterImage = fullInfo.contentDetails?.posterPath?.let { p -> imageUtil.toPosterImageUrl(p) },
        )
    }
}
